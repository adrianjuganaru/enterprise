package se.plushogskolan.jee.exercises.localization;

import java.text.MessageFormat;
import java.text.NumberFormat;
import java.util.Date;
import java.util.Locale;
import java.util.ResourceBundle;


import static java.util.ResourceBundle.Control.*;

public class LocaleExercise {

	public String sayHello(Locale currentLocale) {
		ResourceBundle messages = ResourceBundle.getBundle(
				"localizationexercise", currentLocale);
		return messages.getString("hello");
	}

	public String sayName(String name, Locale currentLocale) {
		ResourceBundle messages = ResourceBundle.getBundle(
				"localizationexercise", currentLocale);
		Object[] messageArguments = { messages.getString("hello"),
				messages.getString("name"), };
		MessageFormat formatter = new MessageFormat("");
		formatter.setLocale(currentLocale);
		formatter.applyPattern(messages.getString("templateName"));
		return formatter.format(messageArguments);
	}

	public String saySomethingEnglish(Locale currentLocale) {

		ResourceBundle bundle = ResourceBundle.getBundle(
				"localizationexercise", new Locale(currentLocale.toString()),
				ResourceBundle.Control.getControl(FORMAT_PROPERTIES));

		return bundle.getString("expectedReply");
	}

	public String introduceYourself(int age, String city, String name,
			Locale currentLocale) {
		ResourceBundle messages = ResourceBundle.getBundle(
				"localizationexercise", currentLocale);
		Object[] messageArguments = { messages.getString("name"),
				messages.getString("age"), messages.getString("city"), };
		MessageFormat formatter = new MessageFormat("");
		formatter.setLocale(currentLocale);

		formatter.applyPattern(messages.getString("templateIntroduction"));
		return formatter.format(messageArguments);
	}
	
	public String sayApointmentDate(Date date, Locale currentLocale){
		
		ResourceBundle messages = ResourceBundle.getBundle(
				"localizationexercise", currentLocale);
		String appointmentDate = messages.getString("appointmentDate");
		MessageFormat form = new MessageFormat(appointmentDate, currentLocale);
		Object[] myDate = new Object[]{date};
		String result = form.format(myDate);
		return result;
	}
	
	public String sayPrice(Double price, Locale currentLocale){
		
		String result = NumberFormat.getCurrencyInstance(currentLocale).format(price);
				
		return result;
	}
	
	public String sayFractionDigits(Double fraction){
		
		String result = NumberFormat.getInstance().format(fraction);
		return result;
	}
}
