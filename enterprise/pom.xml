<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/maven-v4_0_0.xsd">
	<modelVersion>4.0.0</modelVersion>
	<groupId>se.plushogskolan.jee</groupId>
	<artifactId>jee-parent</artifactId>
	<version>1.0-SNAPSHOT</version>
	<packaging>pom</packaging>
	<name>JEE parent</name>

	<modules>
		<module>jetbroker</module>
		<module>utils</module>
		<module>common-war</module>
	</modules>

	<properties>
		<project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
		<org.springframework-version>3.2.1.RELEASE</org.springframework-version>
		<version.jboss.bom>1.0.6.Final</version.jboss.bom>
		<version.surefire.plugin>2.12</version.surefire.plugin>
		<version.jboss.as>7.1.1.Final</version.jboss.as>
		<version.shrinkwrap.resolver>2.0.0-beta-2</version.shrinkwrap.resolver>
	</properties>

	<repositories>
		<repository>
			<id>repository.jboss.org-public</id>
			<name>JBoss repository</name>
			<url>https://repository.jboss.org/nexus/content/groups/public</url>
		</repository>
	</repositories>

	<dependencyManagement>
		<dependencies>



			<!-- Define the version of JBoss' Java EE 6 APIs we want to use -->
			<!-- JBoss distributes a complete set of Java EE 6 APIs including a Bill 
				of Materials (BOM). A BOM specifies the versions of a "stack" (or a collection) 
				of artifacts. We use this here so that we always get the correct versions 
				of artifacts. Here we use the jboss-javaee-6.0 stack (you can read this as 
				the JBoss stack of the Java EE 6 APIs). You can actually use this stack with 
				any version of JBoss AS that implements Java EE 6, not just JBoss AS 7! -->
			<!-- JBoss distributes a complete set of Java EE 6 APIs including a Bill 
				of Materials (BOM). A BOM specifies the versions of a "stack" (or a collection) 
				of artifacts. We use this here so that we always get the correct versions 
				of artifacts. Here we use the jboss-javaee-6.0-with-tools stack (you can 
				read this as the JBoss stack of the Java EE 6 APIs, with some extras tools 
				for your project, such as Arquillian for testing) and the jboss-javaee-6.0-with-hibernate 
				stack you can read this as the JBoss stack of the Java EE 6 APIs, with extras 
				from the Hibernate family of projects) -->
			<dependency>
				<groupId>org.jboss.bom</groupId>
				<artifactId>jboss-javaee-6.0-with-tools</artifactId>
				<version>${version.jboss.bom}</version>
				<type>pom</type>
				<scope>import</scope>
			</dependency>
			<dependency>
				<groupId>org.jboss.bom</groupId>
				<artifactId>jboss-javaee-6.0-with-hibernate</artifactId>
				<version>${version.jboss.bom}</version>
				<type>pom</type>
				<scope>import</scope>
			</dependency>

			<!-- JSR-303 (Bean Validation) Implementation -->
			<!-- Provides portable constraints such as @Email -->
			<!-- Hibernate Validator is shipped in JBoss AS 7 -->
			<dependency>
				<groupId>org.hibernate</groupId>
				<artifactId>hibernate-validator</artifactId>
				<version>4.1.0.Final</version>
				<scope>provided</scope>
				<exclusions>
					<exclusion>
						<groupId>org.slf4j</groupId>
						<artifactId>slf4j-api</artifactId>
					</exclusion>
				</exclusions>
			</dependency>

			<dependency>
				<groupId>joda-time</groupId>
				<artifactId>joda-time</artifactId>
				<version>2.2</version>
			</dependency>


		</dependencies>
	</dependencyManagement>

	<dependencies>

		<dependency>
			<groupId>joda-time</groupId>
			<artifactId>joda-time</artifactId>
			<scope>provided</scope>
		</dependency>

		<!-- Test dependencies -->
		<dependency>
			<groupId>junit</groupId>
			<artifactId>junit</artifactId>
			<type>jar</type>
			<scope>test</scope>
		</dependency>

		<dependency>
			<groupId>org.jboss.shrinkwrap.resolver</groupId>
			<artifactId>shrinkwrap-resolver-api-maven</artifactId>
			<scope>test</scope>
		</dependency>

		<dependency>
			<groupId>org.jboss.shrinkwrap.resolver</groupId>
			<artifactId>shrinkwrap-resolver-impl-maven</artifactId>
			<scope>test</scope>
		</dependency>

		<dependency>
			<groupId>org.jboss.arquillian.extension</groupId>
			<artifactId>arquillian-persistence-impl</artifactId>
			<version>1.0.0.Alpha6</version>
			<scope>test</scope>
		</dependency>

		<dependency>
			<groupId>org.easymock</groupId>
			<artifactId>easymock</artifactId>
			<version>3.2</version>
			<scope>test</scope>
		</dependency>


	</dependencies>

	<build>
		<pluginManagement>
			<plugins>
				<!-- Compiler plugin enforces Java 1.6 compatibility and activates annotation 
					processors -->
				<plugin>
					<artifactId>maven-compiler-plugin</artifactId>
					<version>2.3.2</version>
					<configuration>
						<source>1.6</source>
						<target>1.6</target>
					</configuration>
				</plugin>

				<!-- The JBoss AS plugin deploys your ear to a local JBoss AS container -->
				<plugin>
					<groupId>org.jboss.as.plugins</groupId>
					<artifactId>jboss-as-maven-plugin</artifactId>
					<version>${version.jboss.as}</version>
					<inherited>true</inherited>
					<configuration>
						<skip>true</skip>
					</configuration>
				</plugin>
			</plugins>
		</pluginManagement>

		<plugins>
			<plugin>
				<artifactId>maven-ejb-plugin</artifactId>
				<version>2.3</version>
				<configuration>
					<!-- Tell Maven we are using EJB 3.1 -->
					<ejbVersion>3.1</ejbVersion>
				</configuration>
			</plugin>

			<plugin>
				<artifactId>maven-war-plugin</artifactId>
				<version>2.1.1</version>
				<configuration>
					<!-- Java EE 6 doesn't require web.xml, Maven needs to catch up! -->
					<failOnMissingWebXml>false</failOnMissingWebXml>
				</configuration>
			</plugin>
		</plugins>

	</build>

	<profiles>
		<profile>
			<!-- The default profile skips all tests, though you can tune it to run 
				just unit tests based on a custom pattern -->
			<!-- Seperate profiles are provided for running all tests, including Arquillian 
				tests that execute in the specified container -->
			<id>default</id>
			<activation>
				<activeByDefault>true</activeByDefault>
			</activation>
			<build>
				<plugins>
					<plugin>
						<artifactId>maven-surefire-plugin</artifactId>
						<version>${version.surefire.plugin}</version>
						<configuration>
							<skip>false</skip>
							<includes>
								<include>**/*Test.java</include>
							</includes>
							<excludes>
								<exclude>**/*IntegrationTest.java</exclude>
							</excludes>
						</configuration>
					</plugin>
				</plugins>
			</build>
		</profile>

		<profile>

			<!-- An optional Arquillian testing profile that executes tests in your 
				JBoss AS instance -->
			<!-- This profile will start a new JBoss AS instance, and execute the 
				test, shutting it down when done -->
			<!-- Run with: mvn clean test -Pjboss -->
			<id>jboss</id>
			<dependencies>
				<dependency>
					<groupId>org.jboss.as</groupId>
					<artifactId>jboss-as-arquillian-container-managed</artifactId>
					<scope>test</scope>
				</dependency>
			</dependencies>
			<build>
				<plugins>
					<plugin>
						<artifactId>maven-surefire-plugin</artifactId>
						<version>${version.surefire.plugin}</version>
						<configuration>
							<skip>false</skip>
							<includes>
								<include>**/*IntegrationTest.java</include>
							</includes>
						</configuration>
					</plugin>
				</plugins>
			</build>
		</profile>

		<profile>


			<!-- An optional Arquillian testing profile that executes tests in a remote 
				JBoss AS instance -->
			<!-- Run with: mvn clean test -Pjboss-remote -->
			<id>jboss-remote</id>
			<dependencies>
				<dependency>
					<groupId>org.jboss.as</groupId>
					<artifactId>jboss-as-arquillian-container-remote</artifactId>
					<scope>test</scope>
				</dependency>
			</dependencies>
			<build>
				<plugins>
					<plugin>
						<artifactId>maven-surefire-plugin</artifactId>
						<version>${version.surefire.plugin}</version>
						<configuration>
							<skip>false</skip>
							<includes>
								<include>**/*IntegrationTest.java</include>
							</includes>
						</configuration>
					</plugin>
				</plugins>
			</build>
			
		</profile>

	</profiles>

</project>