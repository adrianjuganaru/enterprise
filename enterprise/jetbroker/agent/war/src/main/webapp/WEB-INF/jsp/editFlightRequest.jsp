<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link href="<%=request.getContextPath()%>/style/common.css"
	type="text/css" rel="stylesheet" />

<title>Jetbroker - Flight Request</title>

<link rel="stylesheet"
	href="//code.jquery.com/ui/1.11.1/themes/smoothness/jquery-ui.css">
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.11.1/jquery-ui.js"></script>
<script>
	$(function() {
		$("#datepicker").datepicker({
			dateFormat : 'dd-mm-yy'
		}).val();
		$("#datepicker").datepicker("showAnim", $(this).fadeIn());
	});
</script>
</head>
<body>

	<h2 class="underline">
		<img src="<%=request.getContextPath()%>/images/flightrequest.png">
		<c:choose>
			<c:when test="${editFlightRequestBean.id > 0}">
				<spring:message code="flightRequest.editFlightRequest" />
			</c:when>
			<c:otherwise>
				<spring:message code="flightRequest.createNewFlightRequest" />
			</c:otherwise>
		</c:choose>
	</h2>

	<form:form commandName="editFlightRequestBean">
		<form:hidden path="id" />
		<table class="formTable">
			<c:if test="${editFlightRequestBean.id > 0}">
				<tr>
					<th><spring:message code="flightRequest.flightRequestID" /></th>
					<td>${editFlightRequestBean.id}</td>
					<td></td>
				</tr>
			</c:if>

			<tr>
				<th><spring:message code="flightRequest.customer" /></th>
				<td><form:select path="customerId">
						<form:option value="-1" label=""></form:option>
						<form:options items="${customers}" itemValue="id"
							itemLabel="fullName" />

					</form:select></td>
				<td><form:errors path="customerId" cssClass="errors" /></td>
			
			</tr>


			<tr>
				<th><spring:message code="flightRequest.departureAirport" /></th>
				<td><form:select path="departureAirport">
						<form:option value=""></form:option>
						<c:forEach items="${airports}" var="airport">
							<form:option path="departureAirport" value="${airport.code}">${airport.name}-
								${airport.code}</form:option>
						</c:forEach>
					</form:select></td>
				<td><form:errors path="departureAirport" cssClass="errors" /></td>
			</tr>

			<tr>
				<th><spring:message code="flightRequest.arrivalAirport" /></th>
				<td><form:select path="arrivalAirport">
						<form:option value=""></form:option>
						<c:forEach items="${airports}" var="airport">
							<form:option path="arrivalAirport" value="${airport.code}">${airport.name}-
								${airport.code}</form:option>
						</c:forEach>
					</form:select></td>
				<td><form:errors path="arrivalAirport" cssClass="errors" /></td>
			</tr>

			<tr>
				<th><spring:message code="flightRequest.passengersNumber" /></th>
				<td><form:input path="passengersNumber" placeholder=""
						required="required" /></td>
				<td><form:errors path="passengersNumber" cssClass="errors" /></td>
			</tr>

			<tr>
				<th><spring:message code="flightRequest.departureDate" /></th>
				<td><form:input type="text" id="datepicker" size="30"
						path="date" /></td>
				<td><form:errors path="date" cssClass="errors" /></td>
			</tr>

			<tr>
				<th><spring:message code="flightRequest.departureTime" /></th>
				<td><form:select path="hour">
						<form:option value="" label="--" />
						<c:forEach begin="0" end="23" var="hour">
							<form:option value="${hour }">${hour }</form:option>
						</c:forEach>
					</form:select> <spring:message code="global.hours" /></td>
				<td><form:errors path="hour" cssClass="errors" /></td>
			</tr>
			<tr>
				<th></th>
				<td><form:select path="minute">
						<form:option value="" label="--" />
						<c:forEach begin="0" end="59" var="min">
							<form:option value="${min }">${min }</form:option>
						</c:forEach>
					</form:select> <spring:message code="global.minutes" /></td>
				<td><form:errors path="minute" cssClass="errors" /></td>
			</tr>

			<tr>
				<th></th>
				<td><c:set var="submitText">
						<spring:message code="global.submit" />
					</c:set> <input type="submit" value="${submitText}" /> <a
					href="<%=request.getContextPath()%>/index.html"><spring:message
							code="global.cancel" /></a></td>
				<td></td>
			</tr>


		</table>
	</form:form>
</body>
</html>