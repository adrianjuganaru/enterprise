<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>

<html>
<head>
<link href="<%=request.getContextPath()%>/style/common.css"
	type="text/css" rel="stylesheet" />

<style type="text/css">
</style>

</head>
<body>
	<jsp:include page="header.jsp" />
	<h2 class="underline">
		<img src="<%=request.getContextPath()%>/images/flightrequest.png"><spring:message code="global.flightRequests"/>
	</h2>
	
	<p><a class="button" href="<%=request.getContextPath()%>/editFlightRequest/0.html"><spring:message code="index.createNewFlightRequest"/></a></p>
	
	<h3><spring:message code="global.requestsWaiting"/></h3>
	
		<table class="dataTable">
		<tr>
			<th><spring:message code="global.id"/></th>
			<th><spring:message code="flightRequest.status"/></th>
			<th><spring:message code="flightRequest.departureAirport"/></th>
			<th><spring:message code="flightRequest.arrivalAirport"/></th>
			<th><spring:message code="flightRequest.date"/></th>
			<th><spring:message code="flightRequest.customer"/></th>
			<th><spring:message code="flightRequest.passengersNumber"/></th>
			<th><spring:message code="global.edit"/></th>
		</tr>		
		<c:forEach items="${flightRequests}" var="flightRequest">
		<tr>
			<td>${flightRequest.id}</td>
			<td>${flightRequest.flightRequestStatus.nicename}</td>
			<td>${flightRequest.departureAirport}</td>
			<td>${flightRequest.arrivalAirport}</td>
			<td><spring:eval expression="flightRequest.dateTime" /></td>
			<td>${flightRequest.customer.fullName}</td>
			<td>${flightRequest.passengersNumber}</td>
			<td><a href="<%=request.getContextPath()%>/editFlightRequest/${flightRequest.id}.html"><img src="images/edit.png" ></a></td>
		</tr>
		</c:forEach>
	</table>
	
	
	<h2 class="underline">
		<img src="<%=request.getContextPath()%>/images/customer.png"><spring:message code="global.customers"/>
	</h2>
	
	<p><a class="button" href="<%=request.getContextPath()%>/editCustomer/0.html"><spring:message code="flightRequest.createNewCustomer"/></a></p>
	
	<table class="dataTable">
		<tr>
			<th><spring:message code="global.id"/></th>
			<th><spring:message code="customer.firstName"/></th>
			<th><spring:message code="customer.lastName"/></th>
			<th><spring:message code="customer.email"/></th>
			<th><spring:message code="customer.company"/></th>
			<th><spring:message code="global.edit"/></th>
		</tr>
		<c:forEach items="${customers}" var="customer">
		<tr>
			<td>${customer.id}</td>
			<td>${customer.firstName}</td>
			<td>${customer.lastName}</td>
			<th>${customer.email}</th>
			<td>${customer.company}</td>
			<td><a href="<%=request.getContextPath()%>/editCustomer/${customer.id}.html"><img src="images/edit.png" ></a></td>
		</tr>
		</c:forEach>
	</table>
	
</body>


</html>