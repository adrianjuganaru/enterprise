package se.plushogskolan.jetbroker.agent.mvc;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import javax.inject.Inject;
import javax.validation.Valid;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import se.plushogskolan.jetbroker.agent.domain.Airport;
import se.plushogskolan.jetbroker.agent.domain.Customer;
import se.plushogskolan.jetbroker.agent.domain.FlightRequest;
import se.plushogskolan.jetbroker.agent.service.CustomerService;
import se.plushogskolan.jetbroker.agent.service.FlightRequestService;
import se.plushogskolan.jetbroker.agent.service.MockAirportService;

@Controller
@RequestMapping("/editFlightRequest/{id}.html")
public class EditFlightRequestController {

	private static Logger log = Logger
			.getLogger(EditFlightRequestController.class.getName());
	@Inject
	private FlightRequestService flightRequestService;
	@Inject
	private MockAirportService mockAirportService;

	@Inject
	private CustomerService customerService;

	@RequestMapping(method = RequestMethod.GET)
	public ModelAndView index(@PathVariable long id) {

		log.fine("Edit flight request, id=" + id);
		EditFlightRequestBean bean = new EditFlightRequestBean();

		if (id > 0) {
			FlightRequest flightRequest = getFlightRequestService()
					.getFlightRequest(id);
			bean.copyFlightRequestValuesToBean(flightRequest);
		}

		return getModelAndView(bean);
	}

	@RequestMapping(method = RequestMethod.POST)
	public ModelAndView handleSubmit(@Valid EditFlightRequestBean bean,
			BindingResult errors) {

		if (errors.hasErrors()) {
			return getModelAndView(bean);
		}

		if (bean.getId() > 0) {
			FlightRequest flightRequest = getFlightRequestService()
					.getFlightRequest(bean.getId());
			bean.copyBeanValuesToFlightRequest(flightRequest);
			flightRequest.setCustomer(getCustomerService().getCustomer(
					bean.getCustomerId()));
			getFlightRequestService().updateFlightRequest(flightRequest);
		} else {
			FlightRequest flightRequest = new FlightRequest();
			bean.copyBeanValuesToFlightRequest(flightRequest);
			flightRequest.setCustomer(getCustomerService().getCustomer(
					bean.getCustomerId()));
			getFlightRequestService().createFlightRequest(flightRequest);
		}

		return new ModelAndView("redirect:/index.html");
	}

	public FlightRequestService getFlightRequestService() {
		return flightRequestService;
	}

	public void setFlightRequestService(
			FlightRequestService flightRequestService) {
		this.flightRequestService = flightRequestService;
	}

	public MockAirportService getMockAirportService() {
		return mockAirportService;
	}

	public void setMockAirportService(MockAirportService mockAirportService) {
		this.mockAirportService = mockAirportService;
	}

	public CustomerService getCustomerService() {
		return customerService;
	}

	public void setCustomerService(CustomerService customerService) {
		this.customerService = customerService;
	}

	/**
	 * A method that has the role of avoiding using the same code in few places
	 * of this project. It populates the bean with the object it needs and it
	 * returns the ModelAndView needed by the controller in order to have all
	 * the elements needed by the jsp page
	 * 
	 * @param bean
	 * @return ModelAndView
	 */
	private ModelAndView getModelAndView(EditFlightRequestBean bean) {
		ModelAndView mav = new ModelAndView("editFlightRequest");
		List<Customer> customers = new ArrayList<Customer>();
		customers = getCustomerService().getAllCustomers();
		List<Airport> airports = new ArrayList<Airport>();
		airports = getMockAirportService().getAllAirports();

		mav.addObject("airports", airports);
		mav.addObject("customers", customers);
		mav.addObject("editFlightRequestBean", bean);

		return mav;
	}

}
