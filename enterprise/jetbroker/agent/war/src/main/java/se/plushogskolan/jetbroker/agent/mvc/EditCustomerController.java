package se.plushogskolan.jetbroker.agent.mvc;

import java.util.logging.Logger;
import javax.inject.Inject;
import javax.validation.Valid;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import se.plushogskolan.jetbroker.agent.domain.Customer;
import se.plushogskolan.jetbroker.agent.mvc.EditCustomerController;
import se.plushogskolan.jetbroker.agent.service.CustomerService;

@Controller
@RequestMapping("/editCustomer/{id}.html")
public class EditCustomerController {
	
	private static Logger log = Logger.getLogger(EditCustomerController.class.getName());
	
	@Inject
	private CustomerService customerService;
	
	@RequestMapping(method = RequestMethod.GET)
	public ModelAndView index(@PathVariable long id) {

		log.fine("Edit customer, id=" + id);

		EditCustomerBean bean = new EditCustomerBean();

		if (id > 0) {
			Customer customer= getCustomerService().getCustomer(id);
			bean.copyCustomerValuesToBean(customer);
		}

		ModelAndView mav = new ModelAndView("editCustomer");
		mav.addObject("editCustomerBean", bean);
		return mav;
	}

	@RequestMapping(method = RequestMethod.POST)
	public ModelAndView handleSubmit(@Valid EditCustomerBean bean, BindingResult errors) {

		if (errors.hasErrors()) {
			ModelAndView mav = new ModelAndView("editCustomer");
			mav.addObject("editCustomerBean", bean);
			return mav;
		}

		if (bean.getId() > 0) {
			Customer customer = getCustomerService().getCustomer(bean.getId());
			bean.copyBeanValuesToCustomer(customer);
			getCustomerService().updateCustomer(customer);
		} else {
			Customer customer = new Customer();
			bean.copyBeanValuesToCustomer(customer);
			getCustomerService().createCustomer(customer);
		}

		return new ModelAndView("redirect:/index.html");
	}
	
	public CustomerService getCustomerService(){
		return customerService;
	}
	
	public void setCustomerService(CustomerService customerService) {
		this.customerService = customerService;
	}
	
	

}
