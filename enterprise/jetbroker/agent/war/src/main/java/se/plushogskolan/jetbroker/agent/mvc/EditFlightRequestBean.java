package se.plushogskolan.jetbroker.agent.mvc;

import javax.validation.constraints.Min;
import javax.validation.constraints.Pattern;
import org.hibernate.validator.constraints.NotEmpty;
import org.hibernate.validator.constraints.Range;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import se.plushogskolan.jetbroker.agent.domain.FlightRequest;

public class EditFlightRequestBean {
	
	
	private long id;

	@Min(value=0, message = "{validation.flightRequest.customer.notSelected}")
	private long customerId;
	@Range(min=1, max=500)
	private int passengersNumber;
	@NotEmpty
	private String departureAirport;
	@NotEmpty
	private String arrivalAirport;
	@NotEmpty
	@Pattern(regexp="^\\d{2}-\\d{2}-\\d{4}$", message = "{validation.flightRequest.date.correctDate}")
	private String date;
	@NotEmpty (message = "{validation.flightRequest.hour.chooseHour}")
	private String hour;
	@NotEmpty (message = "{validation.flightRequest.hour.chooseMinute}")
	private String minute;
	
	public void copyFlightRequestValuesToBean(FlightRequest flightRequest){
		setId(flightRequest.getId());
		setCustomerId(flightRequest.getCustomer().getId());
		setPassengersNumber(flightRequest.getPassengersNumber());
		setDepartureAirport(flightRequest.getDepartureAirport());
		setArrivalAirport(flightRequest.getArrivalAirport());
		setDate(flightRequest.getDateTime().toString(DateTimeFormat.forPattern("dd-MM-yyyy")));
		setHour("" + flightRequest.getDateTime().getHourOfDay());
		setMinute("" + flightRequest.getDateTime().getMinuteOfHour());
	}
	
	public void copyBeanValuesToFlightRequest (FlightRequest flightRequest){
		flightRequest.setId(getId());
		flightRequest.setPassengersNumber(getPassengersNumber());
		flightRequest.setDepartureAirport(getDepartureAirport());
		flightRequest.setArrivalAirport(getArrivalAirport());
		flightRequest.setDateTime(formatDateTime());
		flightRequest.setCustomer(flightRequest.getCustomer());
	}
	
	private DateTime formatDateTime(){
		
		DateTimeFormatter format = DateTimeFormat.forPattern("dd-MM-yyyy HH:mm");
		String dateTimeString = getDate() + " " + getHour() + ":" + getMinute();
		DateTime formatedDateTime = format.parseDateTime(dateTimeString);
		
		return formatedDateTime;
	}
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public long getCustomerId() {
		return customerId;
	}
	public void setCustomerId(long customerId) {
		this.customerId = customerId;
	}
	public int getPassengersNumber() {
		return passengersNumber;
	}
	public void setPassengersNumber(int passengersNumber) {
		this.passengersNumber = passengersNumber;
	}
	public String getDepartureAirport() {
		return departureAirport;
	}
	public void setDepartureAirport(String departureAirport) {
		this.departureAirport = departureAirport;
	}
	public String getArrivalAirport() {
		return arrivalAirport;
	}
	public void setArrivalAirport(String arrivalAirport) {
		this.arrivalAirport = arrivalAirport;
	}
	
	public String getDate() {
		return date;
	}
	
	public void setDate(String date) {
		this.date = date;
	}
	
	public String getHour() {
		return hour;
	}
	
	public void setHour(String hour) {
		this.hour = hour;
	}
	
	public String getMinute() {
		return minute;
	}
	
	public void setMinute(String minute) {
		this.minute = minute;
	}
	

}
