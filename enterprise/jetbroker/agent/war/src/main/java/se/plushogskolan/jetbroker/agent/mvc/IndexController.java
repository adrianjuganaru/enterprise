package se.plushogskolan.jetbroker.agent.mvc;

import java.util.List;
import javax.inject.Inject;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import se.plushogskolan.jetbroker.agent.domain.Airport;
import se.plushogskolan.jetbroker.agent.domain.Customer;
import se.plushogskolan.jetbroker.agent.domain.FlightRequest;
import se.plushogskolan.jetbroker.agent.service.CustomerService;
import se.plushogskolan.jetbroker.agent.service.FlightRequestService;
import se.plushogskolan.jetbroker.agent.service.MockAirportService;

@Controller
public class IndexController {
		
	@Inject
	private CustomerService customerService;
	
	@Inject
	private FlightRequestService flightRequestService;

	@Inject
	private MockAirportService mockAirportService;
	
	@RequestMapping("/index.html")
	public ModelAndView index() {
		
		List<Customer> customers = getCustomerService().getAllCustomers();
		List<FlightRequest> flightRequests = getFlightRequestService().getAllFlightRequests();
		List<Airport> airports = getMockAirportService().getAllAirports();
		ModelAndView mav = new ModelAndView("index");
		
		mav.addObject("customers", customers);
		mav.addObject("flightRequests", flightRequests);
		mav.addObject("airports", airports);
		
		return mav;
	}
	
	public CustomerService getCustomerService(){
		return customerService;
	}
	
	public void setCustomerService(CustomerService customerService){
		this.customerService = customerService;
	}
	
	public FlightRequestService getFlightRequestService(){
		return flightRequestService;
	}
	
	public void setFlightRequestService(FlightRequestService flightRequestService){
		this.flightRequestService = flightRequestService;
	}
	
	public MockAirportService getMockAirportService(){
		return mockAirportService;
	}
	
	public void setMockAirportService(MockAirportService mockAirportService){
		this.mockAirportService = mockAirportService;
	}

	

}
