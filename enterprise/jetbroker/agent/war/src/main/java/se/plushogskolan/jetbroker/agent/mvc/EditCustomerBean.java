package se.plushogskolan.jetbroker.agent.mvc;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotBlank;

import se.plushogskolan.jetbroker.agent.domain.Customer;


public class EditCustomerBean {
	
	private long id;
	@NotBlank
	private String firstName;
	@NotBlank
	private String lastName;
	@NotBlank
	@Email
	private String email;
	private String company;
	
	public void copyCustomerValuesToBean(Customer customer){
		setId(customer.getId());
		setFirstName(customer.getFirstName());
		setLastName(customer.getLastName());
		setEmail(customer.getEmail());
		setCompany(customer.getCompany());
	}
	
	public void copyBeanValuesToCustomer(Customer customer){
		customer.setId(getId());
		customer.setFirstName(getFirstName());
		customer.setLastName(getLastName());
		customer.setEmail(getEmail());
		customer.setCompany(getCompany());
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getCompany() {
		return company;
	}

	public void setCompany(String company) {
		this.company = company;
	}
	
	

}
