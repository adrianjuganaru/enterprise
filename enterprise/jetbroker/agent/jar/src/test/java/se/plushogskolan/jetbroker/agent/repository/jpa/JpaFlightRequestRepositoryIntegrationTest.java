package se.plushogskolan.jetbroker.agent.repository.jpa;

import static org.junit.Assert.assertEquals;
import javax.inject.Inject;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.arquillian.transaction.api.annotation.TransactionMode;
import org.jboss.arquillian.transaction.api.annotation.Transactional;
import org.joda.time.DateTime;
import org.junit.Test;
import org.junit.runner.RunWith;
import se.plushogskolan.jetbroker.TestFixture;
import se.plushogskolan.jetbroker.agent.domain.Customer;
import se.plushogskolan.jetbroker.agent.domain.FlightRequest;
import se.plushogskolan.jetbroker.agent.domain.FlightRequestStatus;
import se.plushogskolan.jetbroker.agent.repository.CustomerRepository;

@RunWith(Arquillian.class)
@Transactional(TransactionMode.ROLLBACK)
public class JpaFlightRequestRepositoryIntegrationTest extends
		AbstractRepositoryTest<FlightRequest, JpaFlightRequestRepository> {

	@Inject
	JpaFlightRequestRepository repo;

	@Inject
	CustomerRepository customerRepo;

	@Test
	public void setAndGetFlightRequest() {

		FlightRequest flightRequest = getEntity1();
		Customer customer = flightRequest.getCustomer();
		repo.persist(flightRequest);
		FlightRequest flightRequest1 = repo.findById(1);
		Customer customerFromDB = flightRequest1.getCustomer();

		assertEquals(FlightRequestStatus.CREATED,
				flightRequest1.getFlightRequestStatus());
		assertEquals(customer.getEmail(), customerFromDB.getEmail());
		assertEquals(new DateTime(2014, 9, 24, 01, 29, 0, 0),
				flightRequest1.getDateTime());

	}

	@Test(expected = Exception.class)
	public void testValidationCustomer() {

		// Create customer with empty lastName. ID must be 0 for integrations
		// tests using JPA.
		Customer customer = TestFixture.getValidCustomer(0, "John", "",
				"john@yahoo.com", "GothenburgCo");
		customerRepo.persist(customer);
	}

	@Test(expected = Exception.class)
	public void testValidationFlightRequest() {

		// Create flightRequest with null customer. ID must be 0 for
		// integrations tests using JPA.
		FlightRequest flightRequest = TestFixture.getValidFlightRequest(0,
				null, 5, "GBG", "STH", new DateTime(2014, 10, 1, 14, 15, 0, 0),
				FlightRequestStatus.CREATED);
		repo.persist(flightRequest);
	}

	@Override
	protected JpaFlightRequestRepository getRepository() {
		return repo;
	}

	@Override
	protected FlightRequest getEntity1() {
		FlightRequest flightRequest = TestFixture.getValidFlightRequest(0);
		customerRepo.persist(flightRequest.getCustomer());
		return flightRequest;
	}

	@Override
	protected FlightRequest getEntity2() {
		FlightRequest flightRequest = TestFixture.getValidFlightRequest(0);
		customerRepo.persist(flightRequest.getCustomer());
		return flightRequest;
	}
}
