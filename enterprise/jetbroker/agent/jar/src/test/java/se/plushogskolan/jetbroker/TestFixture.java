package se.plushogskolan.jetbroker;

import static org.junit.Assert.fail;

import java.util.Set;
import java.util.logging.Logger;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;

import org.jboss.shrinkwrap.api.Archive;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.jboss.shrinkwrap.resolver.api.DependencyResolvers;
import org.jboss.shrinkwrap.resolver.api.maven.MavenDependencyResolver;
import org.joda.time.DateTime;

import se.plushogskolan.jetbroker.agent.domain.Customer;
import se.plushogskolan.jetbroker.agent.domain.FlightRequest;
import se.plushogskolan.jetbroker.agent.domain.FlightRequestStatus;

public class TestFixture {
	
	private static Logger log = Logger.getLogger(TestFixture.class.getName());
	
	public static Customer getValidCustomer(long id, String firstName, String lastName, String email, String company){
		
		Customer customer = new Customer();
		customer.setId(id);
		customer.setFirstName(firstName);
		customer.setLastName(lastName);
		customer.setEmail(email);
		customer.setCompany(company);
		
		return customer;
		
	}
	
	public static FlightRequest getValidFlightRequest(long id, Customer customer, int passengersNumber, String departureAirport, String arrivalAirport, DateTime dateTime, FlightRequestStatus flightRequestStatus){
		FlightRequest flightRequest = new FlightRequest();
		flightRequest.setId(id);
		flightRequest.setCustomer(customer);
		flightRequest.setPassengersNumber(passengersNumber);
		flightRequest.setDepartureAirport(departureAirport);
		flightRequest.setArrivalAirport(arrivalAirport);
		flightRequest.setDateTime(dateTime);
		flightRequest.setFlightRequestStatus(flightRequestStatus);
		
		return flightRequest;
	}

	
	public static Customer getValidCustomer(long id){
		return getValidCustomer(id, "John", "Doe", "john.doe@yahoo.com", "JohnDoeCo");
	}
	
	public static FlightRequest getValidFlightRequest(long id){
		Customer customer = new Customer("John", "Doe", "john.doe@yahoo.com", "JohnDoeCo");
		FlightRequestStatus status = FlightRequestStatus.CREATED;
		DateTime dateTime = new DateTime(2014, 9, 24, 01, 29, 0 ,0);
		return getValidFlightRequest(id, customer, 50, "GBG", "STH", dateTime, status);
	}
	
	public static void assertPropertyIsInvalid(String property, Set<? extends ConstraintViolation<?>> violations) {
	
		boolean errorFound = false;
		for (ConstraintViolation<?> constraintViolation : violations) {
			if (constraintViolation.getPropertyPath().toString()
					.equals(property)) {
				errorFound = true;
				break;
			}
		}

		if (!errorFound) {
			fail("Expected validation error for '" + property
					+ "', but no such error exists");
		}
	}

	
	
	
	

	public static Archive<?> createIntegrationTestArchive() {

		MavenDependencyResolver mvnResolver = DependencyResolvers.use(MavenDependencyResolver.class)
				.loadMetadataFromPom("pom.xml");

		WebArchive war = ShrinkWrap.create(WebArchive.class, "agent_test.war").addPackages(true, "se.plushogskolan")
				.addAsWebInfResource("beans.xml").addAsResource("META-INF/persistence.xml");

		war.addAsLibraries(mvnResolver.artifact("org.easymock:easymock:3.2").resolveAsFiles());
		war.addAsLibraries(mvnResolver.artifact("joda-time:joda-time:2.2").resolveAsFiles());
		war.addAsLibraries(mvnResolver.artifact("org.jadira.usertype:usertype.core:3.1.0.CR8").resolveAsFiles());

		log.info("JAR: " + war.toString(true));
		return war;
	}
}
