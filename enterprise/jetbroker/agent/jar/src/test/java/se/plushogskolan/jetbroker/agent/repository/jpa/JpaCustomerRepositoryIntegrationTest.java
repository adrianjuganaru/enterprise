package se.plushogskolan.jetbroker.agent.repository.jpa;

import static org.junit.Assert.*;

import javax.inject.Inject;

import org.jboss.arquillian.junit.Arquillian;
import org.jboss.arquillian.transaction.api.annotation.TransactionMode;
import org.jboss.arquillian.transaction.api.annotation.Transactional;
import org.junit.Test;
import org.junit.runner.RunWith;
import se.plushogskolan.jetbroker.TestFixture;
import se.plushogskolan.jetbroker.agent.domain.Customer;
import se.plushogskolan.jetbroker.agent.repository.jpa.JpaCustomerRepository;

@RunWith(Arquillian.class)
@Transactional(TransactionMode.ROLLBACK)
public class JpaCustomerRepositoryIntegrationTest extends AbstractRepositoryTest<Customer, JpaCustomerRepository>{

	
	@Inject
	JpaCustomerRepository repo;

	@Test(expected = Exception.class)
	public void testValidationCustomer() {

		// Create customer with empty lastName. ID must be 0 for integrations
		// tests using JPA.
		Customer customer = TestFixture.getValidCustomer(0, "John", "",
				"john@yahoo.com", "GothenburgCo");
		repo.persist(customer);
	}
	@Test
	public void getAllCustomers(){
		Customer customer1 = TestFixture.getValidCustomer(0);
		repo.persist(customer1);
		
		Customer customer2 = TestFixture.getValidCustomer(0);
		repo.persist(customer2);
		
		assertEquals("Number of customers", 2, repo.getAllCustomers().size());
	}
	
	@Override
	protected JpaCustomerRepository getRepository() {
		return repo;
	}

	@Override
	protected Customer getEntity1() {
		return TestFixture.getValidCustomer(0);
	}

	@Override
	protected Customer getEntity2() {
		return TestFixture.getValidCustomer(0);
	}
}
