package se.plushogskolan.jetbroker.agent.domain;

import static org.junit.Assert.*;
import java.util.Set;
import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import org.junit.Test;
import se.plushogskolan.jetbroker.TestFixture;

public class CustomerTest {
	
	@Test
	public void testEmptyCustomerContructor(){
		Customer customer = new Customer();
	
		assertNull(customer.getFirstName());
		assertNull(customer.getLastName());
		assertNull(customer.getEmail());
		assertNull(customer.getCompany());
	}
	
	@Test
	public void testPartialCustomerContructor(){
		Customer customer = new Customer("John", "Doe");
	
		assertEquals("John", customer.getFirstName());	
		assertEquals("Doe", customer.getLastName());
		assertNull(customer.getEmail());
		assertNull(customer.getCompany());
		}
	
	@Test
	public void testFullCustomerContructor(){
		Customer customer = new Customer("John", "Doe", "john.doe@yahoo.com", "JohnDoeCo");

		assertEquals("John", customer.getFirstName());	
		assertEquals("Doe", customer.getLastName());	
		assertEquals("john.doe@yahoo.com", customer.getEmail());
		assertEquals("JohnDoeCo", customer.getCompany());
		}
	
	@Test
	public void testValidation_firstName_empty() {
		Customer customer = TestFixture.getValidCustomer(1);
		customer.setFirstName("");
		Set<ConstraintViolation<Customer>> violations = getValidator().validate(
				customer);
		TestFixture.assertPropertyIsInvalid("firstName", violations);
	}

	@Test
	public void testValidation_firstName_null() {
		Customer customer = TestFixture.getValidCustomer(1);
		customer.setFirstName(null);
		Set<ConstraintViolation<Customer>> violations = getValidator().validate(
				customer);
		TestFixture.assertPropertyIsInvalid("firstName", violations);
	}

	@Test
	public void testValidation_firstName_ok() {
		Customer customer = TestFixture.getValidCustomer(1);
		customer.setFirstName("John");
		Set<ConstraintViolation<Customer>> violations = getValidator().validate(
				customer);
		assertTrue("No validation errors for name", violations.isEmpty());
	}
	
	@Test
	public void testValidation_lastName_empty() {
		Customer customer = TestFixture.getValidCustomer(1);
		customer.setLastName("");
		Set<ConstraintViolation<Customer>> violations = getValidator().validate(
				customer);
		TestFixture.assertPropertyIsInvalid("lastName", violations);
	}

	@Test
	public void testValidation_lastName_null() {
		Customer customer = TestFixture.getValidCustomer(1);
		customer.setLastName(null);
		Set<ConstraintViolation<Customer>> violations = getValidator().validate(
				customer);
		TestFixture.assertPropertyIsInvalid("lastName", violations);
	}
	
	@Test
	public void testValidation_lastName_ok() {
		Customer customer = TestFixture.getValidCustomer(1);
		customer.setLastName("Doe");
		Set<ConstraintViolation<Customer>> violations = getValidator().validate(
				customer);
		assertTrue("No validation errors for name", violations.isEmpty());
	}
	
	@Test
	public void testValidation_email_empty() {
		Customer customer = TestFixture.getValidCustomer(1);
		customer.setEmail("");
		Set<ConstraintViolation<Customer>> violations = getValidator().validate(
				customer);
		TestFixture.assertPropertyIsInvalid("email", violations);
	}

	@Test
	public void testValidation_email_wrong_format() {
		Customer customer = TestFixture.getValidCustomer(1);
		customer.setEmail("wrong format");
		Set<ConstraintViolation<Customer>> violations = getValidator().validate(
				customer);
		TestFixture.assertPropertyIsInvalid("email", violations);
	}
	
	@Test
	public void testValidation_email_null() {
		Customer customer = TestFixture.getValidCustomer(1);
		customer.setEmail(null);
		Set<ConstraintViolation<Customer>> violations = getValidator().validate(
				customer);
		TestFixture.assertPropertyIsInvalid("email", violations);
	}

	@Test
	public void testValidation_email_ok() {
		Customer customer = TestFixture.getValidCustomer(1);
		customer.setEmail("john.doe@mail.jp");
		Set<ConstraintViolation<Customer>> violations = getValidator().validate(
				customer);
		assertTrue("No validation errors for name", violations.isEmpty());
	}
	
	
	protected Validator getValidator() {
		return Validation.buildDefaultValidatorFactory().getValidator();
	}

}
