package se.plushogskolan.jetbroker.agent.domain;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import java.util.Set;
import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import org.joda.time.DateTime;
import org.junit.Test;
import se.plushogskolan.jetbroker.TestFixture;

public class FlightRequestTest {
		
	@Test
	public void testEmptyFlightRequestContructor(){
		FlightRequest flightRequest= new FlightRequest();
		assertNull(flightRequest.getCustomer());
		assertNull(flightRequest.getDepartureAirport());
		assertNull(flightRequest.getArrivalAirport());
		assertNull(flightRequest.getDateTime());
		assertEquals(0, (flightRequest.getPassengersNumber()));
		assertEquals(FlightRequestStatus.CREATED, flightRequest.getFlightRequestStatus());
	}
	
	@Test
	public void testNotEmptyFlightRequestContructor(){
		Customer customer = TestFixture.getValidCustomer(0);
		DateTime date = new DateTime(2012, 12, 15, 11, 30, 0, 0);
		FlightRequest flightRequest = new FlightRequest();
		flightRequest.setCustomer(customer);
		flightRequest.setArrivalAirport("GBG");
		flightRequest.setDepartureAirport("STH");
		flightRequest.setDateTime(date);
		flightRequest.setPassengersNumber(15);
		
		assertEquals(customer, flightRequest.getCustomer());
		assertEquals("GBG", flightRequest.getArrivalAirport());
		assertEquals("STH", flightRequest.getDepartureAirport());
		assertEquals(FlightRequestStatus.CREATED, flightRequest.getFlightRequestStatus());
		assertEquals(15, flightRequest.getPassengersNumber());
		assertEquals(new DateTime(2012, 12, 15, 11, 30, 0, 0), flightRequest.getDateTime());
		}

	@Test
	public void testValidation_flightRequest_customer_null() {
		FlightRequest flightRequest = TestFixture.getValidFlightRequest(1);
		flightRequest.setCustomer(null);
		Set<ConstraintViolation<FlightRequest>> violations = getValidator().validate(
				flightRequest);
		TestFixture.assertPropertyIsInvalid("customer", violations);
	}

	@Test
	public void testValidation_flightRequest_customer_ok() {
		FlightRequest flightRequest = TestFixture.getValidFlightRequest(1);
		Customer customer = new Customer("Donald", "Duck", "donald@duck.com", "Cartoon Network");
		flightRequest.setCustomer(customer);
		Set<ConstraintViolation<FlightRequest>> violations = getValidator().validate(
				flightRequest);
		assertTrue("No validation errors for name", violations.isEmpty());
	}
	
	@Test
	public void testValidation_flightRequest_passengersNumber_underLow() {
		FlightRequest flightRequest = TestFixture.getValidFlightRequest(1);
		flightRequest.setPassengersNumber(0);
		Set<ConstraintViolation<FlightRequest>> violations = getValidator().validate(
				flightRequest);
		TestFixture.assertPropertyIsInvalid("passengersNumber", violations);
	}
	
	@Test
	public void testValidation_flightRequest_passengersNumber_overHigh() {
		FlightRequest flightRequest = TestFixture.getValidFlightRequest(1);
		flightRequest.setPassengersNumber(501);
		Set<ConstraintViolation<FlightRequest>> violations = getValidator().validate(
				flightRequest);
		TestFixture.assertPropertyIsInvalid("passengersNumber", violations);
	}

	@Test
	public void testValidation_flightRequest_passengersNumber_inRange() {
		FlightRequest flightRequest = TestFixture.getValidFlightRequest(1);
		flightRequest.setPassengersNumber(499);
		Set<ConstraintViolation<FlightRequest>> violations = getValidator().validate(
				flightRequest);
		assertTrue("No validation errors for name", violations.isEmpty());
	}
	
	protected Validator getValidator() {
		return Validation.buildDefaultValidatorFactory().getValidator();
	}
	
}
