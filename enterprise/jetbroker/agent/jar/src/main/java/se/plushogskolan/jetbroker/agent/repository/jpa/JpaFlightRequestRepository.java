package se.plushogskolan.jetbroker.agent.repository.jpa;

import java.util.List;
import se.plushogskolan.jee.utils.repository.jpa.JpaRepository;
import se.plushogskolan.jetbroker.agent.domain.FlightRequest;
import se.plushogskolan.jetbroker.agent.repository.FlightRequestRepository;

public class JpaFlightRequestRepository extends JpaRepository<FlightRequest> implements FlightRequestRepository {

	@SuppressWarnings("unchecked")
	@Override
	public List<FlightRequest> getAllFlightRequests() {
		return em.createQuery("SELECT f FROM FlightRequest f").getResultList();
	}
	
}
