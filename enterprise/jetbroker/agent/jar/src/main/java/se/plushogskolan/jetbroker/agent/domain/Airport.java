package se.plushogskolan.jetbroker.agent.domain;

/**
 * This is a simple POJO which simulates a real AIRPORT object (which is due to
 * implement later using integration). It only includes two variables, name and
 * code, and getters and setters for these variables
 * 
 * @author Adrian
 *
 */
public class Airport {

	private String code;
	private String name;

	public Airport() {

	}

	public Airport(String code, String name) {
		setCode(code);
		setName(name);
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return "Airport [code=" + code + ", name=" + name + "]";
	}

}
