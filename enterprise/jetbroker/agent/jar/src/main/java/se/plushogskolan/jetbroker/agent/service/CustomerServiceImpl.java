package se.plushogskolan.jetbroker.agent.service;

import java.util.Collections;
import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;

import se.plushogskolan.jetbroker.agent.domain.Customer;
import se.plushogskolan.jetbroker.agent.service.CustomerService;
import se.plushogskolan.jetbroker.agent.repository.CustomerRepository;

@Stateless
public class CustomerServiceImpl implements CustomerService {
	
	@Inject
	private CustomerRepository customerRepository;

	@Override
	public Customer getCustomer(long id) {
		return getCustomerRepository().findById(id);
	}

	@Override
	public Customer createCustomer(Customer customer) {
		long id = getCustomerRepository().persist(customer);
		return getCustomer(id);
	}

	@Override
	public void updateCustomer(Customer customer) {
		getCustomerRepository().update(customer);
		
	}

	@Override
	public List<Customer> getAllCustomers() {
		List<Customer> customers = getCustomerRepository().getAllCustomers();
		Collections.sort(customers);
		return customers;
	}
	
	public CustomerRepository getCustomerRepository() {
		return customerRepository;
	}

	public void setCustomerRepository(CustomerRepository customerRepository) {
		this.customerRepository = customerRepository;
	}

}
