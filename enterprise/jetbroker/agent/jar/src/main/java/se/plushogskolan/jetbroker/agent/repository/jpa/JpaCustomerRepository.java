package se.plushogskolan.jetbroker.agent.repository.jpa;

import java.util.List;
import se.plushogskolan.jee.utils.repository.jpa.JpaRepository;
import se.plushogskolan.jetbroker.agent.domain.Customer;
import se.plushogskolan.jetbroker.agent.repository.CustomerRepository;

public class JpaCustomerRepository extends JpaRepository<Customer> implements CustomerRepository {

	@SuppressWarnings("unchecked")
	@Override
	public List<Customer> getAllCustomers() {
		return em.createQuery("select c from Customer c").getResultList();
	}

}
