package se.plushogskolan.jetbroker.agent.service;

import java.util.Collections;
import java.util.List;
import java.util.logging.Logger;

import javax.ejb.Stateless;
import javax.inject.Inject;

import se.plushogskolan.jetbroker.agent.domain.FlightRequest;
import se.plushogskolan.jetbroker.agent.domain.FlightRequestStatus;
import se.plushogskolan.jetbroker.agent.repository.FlightRequestRepository;

@Stateless
public class FlightRequestServiceImpl implements FlightRequestService{
	
	@Inject
	private FlightRequestRepository flightRequestRepository;


	@Override
	public FlightRequest getFlightRequest(long id) {
		return getFlightRequestRepository().findById(id);
	}

	@Override
	public FlightRequest createFlightRequest(FlightRequest flightRequest) {
	
		long id = getFlightRequestRepository().persist(flightRequest);
		getFlightRequestRepository().findById(id).setFlightRequestStatus(FlightRequestStatus.CREATED);
		return getFlightRequest(id);
	}

	@Override
	public void updateFlightRequest(FlightRequest flightRequest) {
		getFlightRequestRepository().update(flightRequest);
		
	}

	@Override
	public List<FlightRequest> getAllFlightRequests() {
		List<FlightRequest>flightRequests = getFlightRequestRepository().getAllFlightRequests();
		Collections.sort(flightRequests);
		return flightRequests;
	}

	public FlightRequestRepository getFlightRequestRepository() {
		return flightRequestRepository;
	}
	
	public void setFlightRequestRepository(
			FlightRequestRepository flightRequestRepository) {
		this.flightRequestRepository = flightRequestRepository;
	}
}
