package se.plushogskolan.jetbroker.agent.service;

import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;

import se.plushogskolan.jetbroker.agent.domain.Airport;
import se.plushogskolan.jetbroker.agent.repository.MockAirportRepository;

@Stateless
public class MockAirportServiceImpl implements MockAirportService{
	
	@Inject
	MockAirportRepository mockAirportRepo;


	@Override
	public List<Airport> getAllAirports() {
		List<Airport> airports = getMockAirportRepo().getAllAirports();
		return airports;
	}

	public MockAirportRepository getMockAirportRepo() {
		return mockAirportRepo;
	}
	
	public void setMockAirportRepo(MockAirportRepository mockAirportRepo) {
		this.mockAirportRepo = mockAirportRepo;
	}

}
