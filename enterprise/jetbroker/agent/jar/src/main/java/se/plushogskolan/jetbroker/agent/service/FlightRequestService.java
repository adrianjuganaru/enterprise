package se.plushogskolan.jetbroker.agent.service;

import java.util.List;
import javax.ejb.Local;
import se.plushogskolan.jetbroker.agent.domain.FlightRequest;

@Local
public interface FlightRequestService {
	
	FlightRequest getFlightRequest(long id);
	FlightRequest createFlightRequest(FlightRequest flightRequest);
	void updateFlightRequest(FlightRequest flightRequest);
	List<FlightRequest> getAllFlightRequests();

}
