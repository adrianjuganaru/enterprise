package se.plushogskolan.jetbroker.agent.repository;


import java.util.List;

import se.plushogskolan.jetbroker.agent.domain.Airport;

public interface MockAirportRepository {
	
	public List<Airport> getAllAirports();
	

	
}
