package se.plushogskolan.jetbroker.agent.repository;

import java.util.List;

import se.plushogskolan.jee.utils.repository.BaseRepository;
import se.plushogskolan.jetbroker.agent.domain.FlightRequest;

public interface FlightRequestRepository extends BaseRepository<FlightRequest>{

	List<FlightRequest> getAllFlightRequests();
}
