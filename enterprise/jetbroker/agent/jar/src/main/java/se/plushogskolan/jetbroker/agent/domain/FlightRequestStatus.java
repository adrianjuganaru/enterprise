package se.plushogskolan.jetbroker.agent.domain;

/**
 * A simple enum which contains all the statuses a flight request can have. It
 * also contains methods to show a nicer form of the status (for the tables in
 * user interface)
 * 
 * @author Adrian
 *
 */
public enum FlightRequestStatus {

	CREATED("Created"), REQUEST_CONFIRMED("Request confirmed"), OFFER_RECEIVED(
			"Offer received"), REJECTED("Rejected");

	private final String nicename;

	private FlightRequestStatus(final String nicename) {
		this.nicename = nicename;
	}

	public String getNicename() {
		return nicename;
	}

	@Override
	public String toString() {
		return nicename;
	}
}
