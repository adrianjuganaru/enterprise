package se.plushogskolan.jetbroker.agent.domain;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotBlank;

import se.plushogskolan.jee.utils.domain.IdHolder;

/**
 * A POJO which includes details of a customer, a method to get the full name of
 * the customer and a reference to the list of all the flight requests the
 * customer made (for later implementation)
 * 
 * @author Adrian
 *
 */
@Entity
public class Customer implements IdHolder, Comparable<Customer> {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	@NotBlank
	private String firstName;
	@NotBlank
	private String lastName;
	@NotBlank
	@Email
	private String email;
	private String company;
	@OneToMany(mappedBy = "customer")
	private List<FlightRequest> flightRequests;

	public Customer() {
		this(null, null, null, null);
	}

	public Customer(String firstName, String lastName) {
		this(firstName, lastName, null, null);
	}

	public Customer(String firstName, String lastName, String email,
			String company) {
		setFirstName(firstName);
		setLastName(lastName);
		setEmail(email);
		setCompany(company);
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getCompany() {
		return company;
	}

	public void setCompany(String company) {
		this.company = company;
	}

	public List<FlightRequest> getFlightRequests() {
		return flightRequests;
	}

	public void setFlightRequests(List<FlightRequest> flightRequests) {
		this.flightRequests = flightRequests;
	}

	/**
	 * A method that generates the full name of a customer (as String) by
	 * putting aside first name and last name
	 * 
	 * @return String fullName
	 */
	public String getFullName() {
		String fullName = firstName + " " + lastName;
		return fullName;
	}

	@Override
	public int compareTo(Customer c) {

		String fullName = getFirstName() + " " + getLastName();
		return fullName.compareToIgnoreCase(c.getFullName());
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((email == null) ? 0 : email.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Customer other = (Customer) obj;
		if (email == null) {
			if (other.email != null)
				return false;
		} else if (!email.equals(other.email))
			return false;
		return true;
	}
	
	@Override
	public String toString() {
		return "Customer [id=" + id + ", firstName=" + firstName
				+ ", lastName=" + lastName + ", email=" + email + ", company="
				+ company + "]";
	}

}
