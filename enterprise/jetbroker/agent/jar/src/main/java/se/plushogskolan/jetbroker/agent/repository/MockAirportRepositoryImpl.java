package se.plushogskolan.jetbroker.agent.repository;

import java.util.ArrayList;
import java.util.List;
import se.plushogskolan.jetbroker.agent.domain.Airport;

public class MockAirportRepositoryImpl implements MockAirportRepository{

	private List<Airport> list = new ArrayList<Airport>();
	private Airport airport1= new Airport("GBG", "Gothenburg");
	private Airport airport2= new Airport("STH", "Stockholm");
	private Airport airport3= new Airport("MLM", "Malmo");

	
	public List<Airport> getAllAirports(){
		if(list.isEmpty()){
		list.add(airport1);
		list.add(airport2);
		list.add(airport3);
		}
		return list;
	}
	

}
