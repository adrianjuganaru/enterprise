package se.plushogskolan.jetbroker.agent.domain;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Type;
import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.Range;
import org.joda.time.DateTime;

import se.plushogskolan.jee.utils.domain.IdHolder;

/**
 * A POJO class which includes the details of a flight request and a reference
 * to the customer who wants to make the flight request. The status of the
 * flight request is by default CREATED, but can be later changed to other
 * values as specified in FlightRequestStatus enum
 * 
 * @author Adrian
 *
 */
@Entity
public class FlightRequest implements IdHolder, Comparable<FlightRequest> {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	@NotNull
	@ManyToOne
	private Customer customer;
	@Range(min = 1, max = 500)
	private int passengersNumber;
	@NotBlank
	private String departureAirport;
	@NotBlank
	private String arrivalAirport;
	@NotNull
	@Type(type = "org.jadira.usertype.dateandtime.joda.PersistentDateTime")
	private DateTime dateTime;
	@NotNull
	private FlightRequestStatus flightRequestStatus = FlightRequestStatus.CREATED;

	public FlightRequest() {

	}

	public FlightRequest(long id, Customer customer, int passengersNumber,
			String departureAirport, String arrivalAirport, DateTime dateTime,
			FlightRequestStatus flightRequestStatus) {
		setId(id);
		setCustomer(customer);
		setPassengersNumber(passengersNumber);
		setDepartureAirport(departureAirport);
		setArrivalAirport(arrivalAirport);
		setDateTime(dateTime);
		setFlightRequestStatus(flightRequestStatus);
	}

	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	public int getPassengersNumber() {
		return passengersNumber;
	}

	public void setPassengersNumber(int passengersNumber) {
		this.passengersNumber = passengersNumber;
	}

	public String getDepartureAirport() {
		return departureAirport;
	}

	public void setDepartureAirport(String departureAirport) {
		this.departureAirport = departureAirport;
	}

	public String getArrivalAirport() {
		return arrivalAirport;
	}

	public void setArrivalAirport(String arrivalAirport) {
		this.arrivalAirport = arrivalAirport;
	}

	public DateTime getDateTime() {
		return dateTime;
	}

	public void setDateTime(DateTime dateTime) {
		this.dateTime = dateTime;
	}

	public void setId(long id) {
		this.id = id;
	}

	public long getId() {
		return id;
	}

	public FlightRequestStatus getFlightRequestStatus() {
		return flightRequestStatus;
	}

	public void setFlightRequestStatus(FlightRequestStatus flightRequestStatus) {
		this.flightRequestStatus = flightRequestStatus;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((customer == null) ? 0 : customer.hashCode());
		result = prime * result
				+ ((dateTime == null) ? 0 : dateTime.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		FlightRequest other = (FlightRequest) obj;
		if (customer == null) {
			if (other.customer != null)
				return false;
		} else if (!customer.equals(other.customer))
			return false;
		if (dateTime == null) {
			if (other.dateTime != null)
				return false;
		} else if (!dateTime.equals(other.dateTime))
			return false;
		return true;
	}

	@Override
	public int compareTo(FlightRequest c) {
		
		return 0;
	}
	@Override
	public String toString() {
		return "FlightRequest [id=" + id + ", customer=" + customer
				+ ", passengersNumber=" + passengersNumber
				+ ", departureAirport=" + departureAirport
				+ ", arrivalAirport=" + arrivalAirport + ", dateTime="
				+ dateTime + ", flightRequestStatus=" + flightRequestStatus
				+ "]";
	}


}
