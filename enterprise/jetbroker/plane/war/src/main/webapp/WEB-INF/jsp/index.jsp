<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>

<html>
<head>

<link href="<%=request.getContextPath()%>/style/common.css" type="text/css" rel="stylesheet"  />

</head>
<body>
	
	<jsp:include page="header.jsp" />

	<h2 class="underline">
		<img src="<%=request.getContextPath()%>/images/plane.png">
		<spring:message code="global.planeTypes"/>
	</h2>
	

	<p><a class="button" href="<%=request.getContextPath()%>/editPlaneType/0.html"><spring:message code="index.createNewPlaneType"/></a></p>

	<table class="dataTable">
		<tr>
			<th><spring:message code="global.id"/></th>
			<th><spring:message code="flightrequest.code"/></th>
			<th><spring:message code="global.name"/></th>
			<th><spring:message code="flightrequest.noOfPassengers"/></th>
			<th><spring:message code="flightrequest.range"/></th>
			<th><spring:message code="flightrequest.maxSpeed"/></th>
			<th><spring:message code="flightrequest.fuelConsumption"/></th>
			<th><spring:message code="global.edit"/></th>
		</tr>
		<c:forEach items="${planeTypes}" var="planeType">
		<tr>
			<td>${planeType.id}</td>
			<td>${planeType.code}</td>
			<td>${planeType.name}</td>
			<td>${planeType.maxNoOfPassengers}</td>
			<td>${planeType.maxRangeKm}</td>
			<td>${planeType.maxSpeedKmH}</td>
			<td>${planeType.fuelConsumptionPerKm}</td>
			<td><a href="<%=request.getContextPath()%>/editPlaneType/${planeType.id}.html"><img src="images/edit.png" ></a></td>
		</tr>
		</c:forEach>
	</table>
	
	<h2 class="underline">
		<img src="<%=request.getContextPath()%>/images/airport.png">
		<spring:message code="global.airports"/>
	</h2>
	
	<p>
		<a class="button" href="<%=request.getContextPath()%>/editAirport/0.html"><spring:message code="index.createNewAirport"/></a>
	</p>

	<table class="dataTable">
		<tr>
			<th><spring:message code="global.id"/></th>
			<th><spring:message code="airport.code"/></th>
			<th><spring:message code="global.name"/></th>
			<th><spring:message code="airport.latitude"/></th>
			<th><spring:message code="airport.longitude"/></th>
			<th><spring:message code="global.edit"/></th>
		</tr>
		<c:forEach items="${airports}" var="airport">
		<tr>
			<td>${airport.id}</td>
			<td>${airport.code}</td>
			<td>${airport.name}</td>
			<td>${airport.latitude}</td>
			<td>${airport.longitude}</td>
			<td><a href="<%=request.getContextPath()%>/editAirport/${airport.id}.html"><img src="images/edit.png"></a></td>
		</tr>
		</c:forEach>
	</table>
	
	<h2 class="underline">
		<img src="<%=request.getContextPath()%>/images/gas.png">
		<spring:message code="global.fuelPrice"/>
	</h2>
	
	<p><spring:message code="index.current.fuelCost" arguments="${fuelCost}" /> SEK. <a href="<%=request.getContextPath()%>/editFuelCost.html"><img src="images/edit.png"></a>

</body>

</html>