package se.plushogskolan.jetbroker.plane.service;

import java.util.Collections;
import java.util.List;
import java.util.logging.Logger;

import javax.ejb.Stateless;
import javax.inject.Inject;

import se.plushogskolan.jetbroker.plane.domain.Airport;
import se.plushogskolan.jetbroker.plane.repository.AirportRepository;

@Stateless
public class AirportServiceImpl implements AirportService {

	private static Logger log  = Logger.getLogger(AirportServiceImpl.class.getName());

	private static double cachedFuelCost = 5.2;

	@Inject
	private AirportRepository airportRepo;

	@Override
	public Airport getAirport(long id) {
		return getAirportRepo().findById(id);
	}

	@Override
	public Airport getAirportByCode(String code) {
		return getAirportRepo().getAirportByCode(code);
	}

	@Override
	public void updateAirport(Airport airport) {
		getAirportRepo().update(airport);
	}

	@Override
	public Airport createAirport(Airport airport) {
		long id = getAirportRepo().persist(airport);
		return getAirport(id);
	}

	@Override
	public List<Airport> getAllAirports() {
		List<Airport> airports = getAirportRepo().getAllAirports();
		Collections.sort(airports);
		return airports;
	}

	public AirportRepository getAirportRepo() {
		return airportRepo;
	}

	public void setAirportRepo(AirportRepository airportRepo) {
		this.airportRepo = airportRepo;
	}

	@Override
	public double getFuelCost() {
		return cachedFuelCost;
	}

	@Override
	public void updateFuelCost(double fuelCost) throws Exception {
		cachedFuelCost = fuelCost;

	}


}
