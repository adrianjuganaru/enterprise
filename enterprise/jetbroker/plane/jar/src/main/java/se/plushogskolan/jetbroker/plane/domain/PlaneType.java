package se.plushogskolan.jetbroker.plane.domain;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.Range;

@Entity
public class PlaneType {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	@NotBlank
	private String code;
	@NotBlank
	private String name;
	@Range(min = 1, max = 600)
	private int maxNoOfPassengers;
	@Range(min = 1, max = 100000)
	private int maxRangeKm;
	@Range(min = 150, max = 1200)
	private int maxSpeedKmH;
	@Range(min = 1, max = 1000)
	private double fuelConsumptionPerKm;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getMaxNoOfPassengers() {
		return maxNoOfPassengers;
	}

	public void setMaxNoOfPassengers(int maxNoOfPassengers) {
		this.maxNoOfPassengers = maxNoOfPassengers;
	}

	public int getMaxRangeKm() {
		return maxRangeKm;
	}

	public void setMaxRangeKm(int maxRangeKm) {
		this.maxRangeKm = maxRangeKm;
	}

	public int getMaxSpeedKmH() {
		return maxSpeedKmH;
	}

	public void setMaxSpeedKmH(int maxSpeedKmH) {
		this.maxSpeedKmH = maxSpeedKmH;
	}

	public double getFuelConsumptionPerKm() {
		return fuelConsumptionPerKm;
	}

	public void setFuelConsumptionPerKm(double fuelConsumptionPerKm) {
		this.fuelConsumptionPerKm = fuelConsumptionPerKm;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

}
