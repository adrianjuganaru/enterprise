<%@ page contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<html>
<head>
<title>Edit Flight Request</title>

<link href="<%=request.getContextPath()%>/style/common.css"
	type="text/css" rel="stylesheet" />

<script src="<%=request.getContextPath()%>/js/jquery-1.9.1.js"></script>
<script
	src="<%=request.getContextPath()%>/js/jquery-ui-1.10.3.custom.js"></script>
<link rel="stylesheet"
	href="<%=request.getContextPath()%>/js/jquery-ui-1.10.3.custom.css" />

<script>
	$(document).ready(function() {

		$(function() {
			$("#datepicker").datepicker({
				dateFormat : 'dd-mm-yy'
			});
		});

	});
</script>

</head>

<body>

	<jsp:include page="../header.jsp" />

	<h2 class="underline"> <img src="<%=request.getContextPath()%>/images/flightrequest.png">
	<c:choose>
		<c:when test="${isNewRequest}">
			Add new flight request
		</c:when>
		<c:otherwise>
			Edit flight request
		</c:otherwise>
	</c:choose>
	</h2>


	<form:form commandName="editFlightRequestBean">
	
		<form:errors path="" cssClass="errors" element="div" />
		
		<form:hidden path="id" />
		<table class="formTable">
			<c:if test="${not isNewRequest}">
				<tr>
					<th>ID</th>
					<td>${editFlightRequestBean.id}</td>
					<td></td>
				</tr>
			</c:if>
			<tr>
				<th>Customer</th>
				<td><form:select path="customerId">
						<form:option value="-1" label="" />
						<form:options items="${customers}" itemValue="id"
							itemLabel="fullName" />
					</form:select></td>
				<td><form:errors path="customerId" cssClass="errors" /></td>
			</tr>
			<tr>
				<th>Departure airport</th>
				<td><form:select path="departureAirportCode">
						<form:option value=""></form:option>
						<form:options items="${airports}" itemValue="code"
							itemLabel="name" />
					</form:select></td>
				<td><form:errors path="departureAirportCode"
						cssClass="errors" /></td>
			</tr>
			<tr>
				<th>Arrival airport</th>
				<td><form:select path="arrivalAirportCode">
						<form:option value=""></form:option>
						<form:options items="${airports}" itemValue="code"
							itemLabel="name" />
					</form:select></td>
				<td><form:errors path="arrivalAirportCode"
						cssClass="errors" /></td>
			</tr>
			<tr>
				<th>No of passengers</th>
				<td><form:input path="noOfPassengers" size="3" />
				</td>
				<td><form:errors path="noOfPassengers"
						cssClass="errors" /></td>
			</tr>
			<tr>
				<th>Departure date</th>
				<td><form:input path="date" id="datepicker" /></td>
				<td><form:errors path="date" cssClass="errors" /></td>
			</tr>
			<tr>
				<th rowspan="2">Departure time</th>
				<td><form:select path="hour">
						<form:option value="-1" label="" />
						<form:options items="${hours}" />
					</form:select> hour</td>
				<td><form:errors path="hour" cssClass="errors" />
				</td>
			</tr>
			<tr>
				<td><form:select path="minute">
						<form:option value="-1" label=""></form:option>
						<form:options items="${minutes}" />
					</form:select> minute</td>
				<td><form:errors path="minute" cssClass="errors" /></td>
			</tr>


			<tr>
				<th></th>
				<td><input type="submit" value="Submit" /> <a href="<%=request.getContextPath()%>/index.html">Cancel</a></td>
				<td></td>
			</tr>

		</table>
	</form:form>

</body>
</html>