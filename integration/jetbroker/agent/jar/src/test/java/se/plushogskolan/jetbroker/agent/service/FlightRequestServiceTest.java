package se.plushogskolan.jetbroker.agent.service;

import org.easymock.Capture;
import org.easymock.EasyMock;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;
import se.plushogskolan.jetbroker.agent.TestFixture;
import se.plushogskolan.jetbroker.agent.domain.request.FlightRequest;
import se.plushogskolan.jetbroker.agent.domain.request.FlightRequestConfirmation;
import se.plushogskolan.jetbroker.agent.domain.request.FlightRequestStatus;
import se.plushogskolan.jetbroker.agent.integration.order.OrderFacade;
import se.plushogskolan.jetbroker.agent.repository.FlightRequestRepository;
import se.plushogskolan.jetbroker.agent.services.FlightRequestServiceImp;
import static org.easymock.EasyMock.*;  

public class FlightRequestServiceTest {
	
	private FlightRequest flightRequest;
	
	
	@Before
	public void setup(){
		flightRequest = TestFixture.getValidFlightRequest();
		
	}

	
	@Test
	public void testCreateFlightRequest() throws Exception{
				
		flightRequest.setRequestStatus(FlightRequestStatus.REJECTED);//setting up incorrect value
		
		Capture<FlightRequest> capturedFlightRequest = new Capture<FlightRequest>();
			
		FlightRequestRepository flightRequestRepositoryMock = createMock(FlightRequestRepository.class);
		expect(flightRequestRepositoryMock.persist(EasyMock.capture(capturedFlightRequest))).andReturn(5L);
		expect(flightRequestRepositoryMock.findById(5L)).andReturn(flightRequest);
		OrderFacade orderFacadeMock = createMock(OrderFacade.class);
		orderFacadeMock.sendFlightRequest(flightRequest);
	
		replay(flightRequestRepositoryMock);
		replay(orderFacadeMock);
		
		FlightRequestServiceImp flightRequestServiceImpl = new FlightRequestServiceImp();
		flightRequestServiceImpl.setRepository(flightRequestRepositoryMock);
		flightRequestServiceImpl.setOrderFacade(orderFacadeMock);
		flightRequestServiceImpl.createFlightRequest(flightRequest);
		
		verify(flightRequestRepositoryMock);
		verify(orderFacadeMock);
		
		assertEquals(FlightRequestStatus.CREATED, capturedFlightRequest.getValue().getRequestStatus());

	}
	
	@Test
	public void testHandleFlightRequestConfirmation() throws Exception{
		
		Capture<FlightRequest> capturedFlightRequest = new Capture<FlightRequest>();
	
		FlightRequestConfirmation confirmationMock = createMock(FlightRequestConfirmation.class);
		EasyMock.expect(confirmationMock.getAgentRequestId()).andReturn(2L).anyTimes();
		EasyMock.expect(confirmationMock.getOrderRequestId()).andReturn(3L).anyTimes();

		EasyMock.replay(confirmationMock);

		
		FlightRequestRepository flightRequestRepositoryMock = createMock(FlightRequestRepository.class);
		EasyMock.expect(flightRequestRepositoryMock.findById(confirmationMock.getAgentRequestId())).andReturn(flightRequest);
		flightRequestRepositoryMock.update(EasyMock.capture(capturedFlightRequest));
		
		EasyMock.replay(flightRequestRepositoryMock);
		
		FlightRequestServiceImp flightRequestServiceImpl = new FlightRequestServiceImp();
		flightRequestServiceImpl.setRepository(flightRequestRepositoryMock);
		flightRequestServiceImpl.handleFlightRequestConfirmation(confirmationMock);
		
		verify(flightRequestRepositoryMock);
		verify(confirmationMock);
		
		assertEquals(FlightRequestStatus.REQUEST_CONFIRMED, capturedFlightRequest.getValue().getRequestStatus());
		assertEquals(3, capturedFlightRequest.getValue().getConfirmationId());
	}
	
	
	

	
}
