package se.plushogskolan.jetbroker.agent.integration.order.mock.mdb;

import java.util.logging.Logger;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.event.Observes;
import javax.inject.Inject;

import se.plushogskolan.jee.utils.cdi.Mock;
import se.plushogskolan.jetbroker.agent.annotations.FlightRequestChanged;
import se.plushogskolan.jetbroker.agent.annotations.FlightRequestChanged.ChangeType;
import se.plushogskolan.jetbroker.agent.events.FlightRequestChangedEvent;
import se.plushogskolan.jetbroker.agent.services.FlightRequestService;

@ApplicationScoped
public class MockFlightRequestRejectionMDB {

	private static Logger log = Logger.getLogger(MockFlightRequestRejectionMDB.class.getName());

	@Inject
	private FlightRequestService flightRequestService;

	public void handleEvent(@Observes @FlightRequestChanged(ChangeType.REJECTED) @Mock FlightRequestChangedEvent event) {

		log.fine("FlightRequest is rejected in mock.");
		getFlightRequestService().handleFlightRequestRejection(event.getFlightRequestId());
	}

	public FlightRequestService getFlightRequestService() {
		return flightRequestService;
	}

	public void setFlightRequestService(FlightRequestService flightRequestService) {
		this.flightRequestService = flightRequestService;
	}

}
