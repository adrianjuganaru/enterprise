package se.plushogskolan.jetbroker.agent.integration.order.impl.mdb;

import java.util.logging.Logger;

import javax.ejb.ActivationConfigProperty;
import javax.ejb.MessageDriven;
import javax.inject.Inject;
import javax.jms.Message;
import javax.jms.MessageListener;

import se.plushogskolan.jee.utils.jms.AbstractMDB;
import se.plushogskolan.jee.utils.jms.JmsConstants;
import se.plushogskolan.jetbroker.agent.domain.request.FlightRequestConfirmation;
import se.plushogskolan.jetbroker.agent.services.FlightRequestService;

@MessageDriven(name = "FlightRequestConfirmationMDB", activationConfig = {
		@ActivationConfigProperty(propertyName = "destinationType", propertyValue = "javax.jms.Queue"),
		@ActivationConfigProperty(propertyName = "destination", propertyValue = JmsConstants.QUEUE_FLIGHTREQUEST_RESPONSE),
		@ActivationConfigProperty(propertyName = "messageSelector", propertyValue = "messageType = 'Confirmation'")

})
public class FlightRequestConfirmationMDB extends AbstractMDB implements MessageListener {

	private static Logger log = Logger.getLogger(FlightRequestConfirmationMDB.class.getName());

	
	@Inject
	private FlightRequestService flightRequestService;

	@Override
	public void onMessage(Message message) { 
		
		log.fine("entering flightRequestConfirmationMDB. message is " + message.toString());
		
		try {
			long agentId = message.getLongProperty("agentID");
			log.fine("flightRequestConfirmationMDB - agent Id is " + agentId);
			long confirmationId = message.getLongProperty("confirmationID");
			log.fine("flightRequestConfirmationMDB - confirmation Id is " + confirmationId);
			
			FlightRequestConfirmation confirmation = new FlightRequestConfirmation(agentId, confirmationId);
			log.fine("flightRequestConfirmationMDB - confirmation is " + confirmation.toString());
			getFlightRequestService().handleFlightRequestConfirmation(confirmation);
		} catch (Exception e) {
			logErrorAndRollback(e);

		} 
	}
		
	public FlightRequestService getFlightRequestService() {
		return flightRequestService;
	}

	public void setFlightRequestService(FlightRequestService flightRequestService) {
		this.flightRequestService = flightRequestService;
	}


}
