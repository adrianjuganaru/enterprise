package se.plushogskolan.jetbroker.agent.repository.jpa;

import java.util.List;

import javax.persistence.Query;

import se.plushogskolan.jee.utils.repository.jpa.JpaRepository;
import se.plushogskolan.jetbroker.agent.domain.Customer;
import se.plushogskolan.jetbroker.agent.repository.CustomerRepository;

@SuppressWarnings("unchecked")
public class JpaCustomerRepository extends JpaRepository<Customer> implements CustomerRepository {

	@Override
	public List<Customer> getAllCustomers() {

		Query query = em.createQuery("select c from Customer c");
		return query.getResultList();

	}

}
