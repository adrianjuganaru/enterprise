package se.plushogskolan.jetbroker.agent.integration.order.impl;

import java.text.SimpleDateFormat;
import java.util.logging.Logger;
import javax.annotation.Resource;
import javax.jms.MapMessage;
import javax.jms.Queue;
import javax.jms.QueueConnection;
import javax.jms.QueueConnectionFactory;
import javax.jms.QueueSender;
import javax.jms.QueueSession;
import javax.jms.Session;
import se.plushogskolan.jee.utils.cdi.Prod;
import se.plushogskolan.jee.utils.jms.JmsConstants;
import se.plushogskolan.jee.utils.jms.JmsHelper;
import se.plushogskolan.jetbroker.agent.domain.request.FlightRequest;
import se.plushogskolan.jetbroker.agent.integration.order.OrderFacade;

@Prod
public class OrderFacadeImpl implements OrderFacade {

	private static Logger log = Logger.getLogger(OrderFacadeImpl.class.getName());
	
	@Resource(mappedName = JmsConstants.JNDI_CONNECTIONFACTORY)
	private QueueConnectionFactory connectionFactory;
	
	@Resource(mappedName = JmsConstants.QUEUE_FLIGHTREQUEST_REQUEST)
	private Queue queue;
	

	public void sendFlightRequest(FlightRequest request) throws Exception {
		QueueConnection connection = null;
		QueueSession senderSession = null;
		
		log.fine("entering sendFlightRequest");
		
		try{
		connection = connectionFactory.createQueueConnection();
		senderSession = connection.createQueueSession(false, Session.AUTO_ACKNOWLEDGE);
		connection.start();
		
		MapMessage message = senderSession.createMapMessage();
		message.setStringProperty("messageType", "flightRequestQueue");
		message.setIntProperty("noOfPassengers", request.getNoOfPassengers());
		message.setStringProperty("departureAirportCode", request.getDepartureAirportCode());
		message.setStringProperty("arrivalAirportCode", request.getArrivalAirportCode());
		message.setStringProperty("departureTime", new SimpleDateFormat("dd-MM-yyyy HH:mm:ss").format(request.getDepartureTime().toDate()));
		message.setLongProperty("agentRequestId", request.getId());
		QueueSender sender = senderSession.createSender(queue);
		sender.send(message);

		log.fine("sendFlightRequest in OrderFacade IMPL: the message is " + message.toString());
		log.fine("sendFlightRequest in OrderFacade IMPL: " + request.toString());
		
		}catch(Exception e){
			throw new RuntimeException(e);
		} finally {
			JmsHelper.closeConnectionAndSession(connection, senderSession);
		}
		
		
	}
	
	


}
