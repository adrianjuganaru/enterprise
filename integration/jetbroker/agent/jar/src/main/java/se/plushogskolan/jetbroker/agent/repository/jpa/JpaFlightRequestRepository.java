package se.plushogskolan.jetbroker.agent.repository.jpa;

import java.util.List;

import javax.persistence.Query;

import se.plushogskolan.jee.utils.repository.jpa.JpaRepository;
import se.plushogskolan.jetbroker.agent.domain.request.FlightRequest;
import se.plushogskolan.jetbroker.agent.repository.FlightRequestRepository;

@SuppressWarnings("unchecked")
public class JpaFlightRequestRepository extends JpaRepository<FlightRequest> implements FlightRequestRepository {

	@Override
	public List<FlightRequest> getAllFlightRequests() {
		Query query = em.createQuery("select f from FlightRequest f");
		return query.getResultList();
	}

	@Override
	public List<FlightRequest> getFlightRequestsForCustomer(long id) {
		Query query = em.createQuery("select f from FlightRequest f where f.customer.id=?1");
		query.setParameter(1, id);
		return query.getResultList();
	}

}
