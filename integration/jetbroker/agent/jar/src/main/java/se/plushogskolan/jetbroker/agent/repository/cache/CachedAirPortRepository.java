package se.plushogskolan.jetbroker.agent.repository.cache;

import java.util.List;
import java.util.logging.Logger;
import javax.ejb.Stateless;
import javax.inject.Inject;
import se.plushogskolan.jee.utils.cdi.Prod;
import se.plushogskolan.jetbroker.agent.domain.AirPort;
import se.plushogskolan.jetbroker.agent.integration.plane.PlaneFacade;
import se.plushogskolan.jetbroker.agent.repository.AirPortRepository;

@Stateless
public class CachedAirPortRepository implements AirPortRepository {

	@Inject
	Logger log;

	@Inject @Prod
	private PlaneFacade planeFacade;

	@Override
	public List<AirPort> getAllAirPorts() {
		
		return planeFacade.getAllAirports();
			
	}

	@Override
	public AirPort getAirPort(String code) {

		for (AirPort airPort : planeFacade.getAllAirports()) {
			if (airPort.getCode().equals(code)) {
				return airPort;
			}
		}

		log.info("No airport found for code " + code);
		return null;

	}

	@Override
	public void handleAirportsChangedEvent() {
		log.fine("Received airports changed event. ");
	}

}
