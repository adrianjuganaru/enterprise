package se.plushogskolan.jetbroker.agent.repository.cache;

import java.util.List;
import java.util.logging.Logger;
import javax.ejb.Stateless;
import javax.inject.Inject;
import se.plushogskolan.jee.utils.cdi.Prod;
import se.plushogskolan.jetbroker.agent.domain.PlaneType;
import se.plushogskolan.jetbroker.agent.integration.plane.PlaneFacade;
import se.plushogskolan.jetbroker.agent.repository.PlaneTypeRepository;

@Stateless
public class CachedPlaneTypeRepository implements PlaneTypeRepository {
	@Inject
	Logger log;

	@Inject @Prod
	private PlaneFacade planeFacade;

	@Override
	public List<PlaneType> getAllPlaneTypes() {
		return planeFacade.getAllPlaneTypes();
	}

	@Override
	public PlaneType getPlaneType(String code) {

		List<PlaneType> planeTypes = planeFacade.getAllPlaneTypes();
		for (PlaneType planeType : planeTypes) {

			if (planeType.getCode().equals(code)) {
				return planeType;
			}
		}

		log.info("No plane type found for code " + code);
		return null;
	}

	@Override
	public void handlePlaneTypeChangedEvent() {
		log.fine("Handle plane types changed event");

	}

}
