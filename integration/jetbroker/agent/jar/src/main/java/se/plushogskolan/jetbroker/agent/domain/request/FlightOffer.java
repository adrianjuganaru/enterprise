package se.plushogskolan.jetbroker.agent.domain.request;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;


@Entity
public class FlightOffer {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	@OneToOne(mappedBy = "offer")
	private FlightRequest request;
	private double offeredPrice;
	private String planeTypeCode;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public FlightRequest getRequest() {
		return request;
	}

	public void setRequest(FlightRequest request) {
		this.request = request;
	}

	public double getOfferedPrice() {
		return offeredPrice;
	}

	public void setOfferedPrice(double offeredPrice) {
		this.offeredPrice = offeredPrice;
	}

	public String getPlaneTypeCode() {
		return planeTypeCode;
	}

	public void setPlaneTypeCode(String planeTypeCode) {
		this.planeTypeCode = planeTypeCode;
	}

}
