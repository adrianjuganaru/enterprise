package se.plushogskolan.jetbroker.agent.integration.plane.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import javax.inject.Inject;

import se.plushogskolan.jee.utils.cdi.Prod;
import se.plushogskolan.jetbroker.agent.domain.AirPort;
import se.plushogskolan.jetbroker.agent.domain.PlaneType;
import se.plushogskolan.jetbroker.agent.integration.plane.PlaneFacade;
import se.plushogskolan.jetbroker.agent.integration.plane.ws.stubs.PlaneWebService;
import se.plushogskolan.jetbroker.agent.integration.plane.ws.stubs.PlaneWebServiceImplService;
import se.plushogskolan.jetbroker.agent.integration.plane.ws.stubs.WsAirport;
import se.plushogskolan.jetbroker.agent.integration.plane.ws.stubs.WsPlaneType;

@Prod
public class PlaneFacadeImpl implements PlaneFacade {
	
	@Inject
	Logger log;

	@Override
	public List<AirPort> getAllAirports() {

		PlaneWebServiceImplService planeWebServiceImplService = new PlaneWebServiceImplService();
		PlaneWebService planeWebServicePort = planeWebServiceImplService.getPlaneWebServicePort();
		List<WsAirport> wsAirports = planeWebServicePort.getAirports().getItem();
		List<AirPort> airports = new ArrayList<AirPort>();
		log.fine("PlaneFacadeImpl entering. Nr of wsAirports is: " + wsAirports.size());
		for(WsAirport wsAirport : wsAirports){
			AirPort airport = new AirPort();
			airport.setCode(wsAirport.getCode());
			airport.setName(wsAirport.getName());
			airports.add(airport);
		}
		
		log.fine("PlaneFacadeImpl: getAllAirports returns: " + airports.toString());
		
		return airports;

	}

	@Override
	public List<PlaneType> getAllPlaneTypes() {
		PlaneWebServiceImplService planeWebServiceImplService = new PlaneWebServiceImplService();
		List<PlaneType> planeTypes = new ArrayList<PlaneType>();
		PlaneWebService planeWebServicePort = planeWebServiceImplService.getPlaneWebServicePort();
		List<WsPlaneType> wsPlaneTypes = planeWebServicePort.getPlaneTypes().getItem();
		PlaneType planeType = new PlaneType();
		
		for (WsPlaneType wsPlaneType : wsPlaneTypes){
			planeType.setCode(wsPlaneType.getCode());
			planeType.setFuelConsumptionPerKm(wsPlaneType.getFuelConsumptionPerKm());
			planeType.setMaxNoOfPassengers(wsPlaneType.getMaxNoOfPassengers());
			planeType.setMaxRangeKm(wsPlaneType.getMaxRangeKm());
			planeType.setMaxSpeedKmH(wsPlaneType.getMaxSpeedKmH());
			planeType.setName(wsPlaneType.getName());
			planeTypes.add(planeType);
		}

//		PlaneType type1 = new PlaneType();
//		type1.setCode("B770");
//		type1.setName("Boeing 770 [Mocked]");
//		type1.setFuelConsumptionPerKm(200);
//		type1.setMaxNoOfPassengers(300);
//		type1.setMaxRangeKm(700);
//		type1.setMaxSpeedKmH(980);
//		type1.setFuelConsumptionPerKm(34);
//		planeTypes.add(type1);
//
//		PlaneType type2 = new PlaneType();
//		type2.setCode("A450");
//		type2.setName("Airbus 450 [Mocked]");
//		type2.setFuelConsumptionPerKm(140);
//		type2.setMaxNoOfPassengers(270);
//		type2.setMaxRangeKm(600);
//		type2.setMaxSpeedKmH(680);
//		type2.setFuelConsumptionPerKm(22);
//		planeTypes.add(type2);

		return planeTypes;
	}

}
