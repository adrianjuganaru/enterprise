package se.plushogskolan.jetbroker.agent.rest.exercises.xml.xmlhandlers;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import se.plushogskolan.jee.utils.xml.XmlUtil;
import se.plushogskolan.jetbroker.agent.domain.request.FlightRequest;

public class DomBuilder {

	public static String buildXmlUsingDom(FlightRequest fr) throws Exception {

		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder = factory.newDocumentBuilder();

		Document doc = builder.newDocument();
		
		Element flightrequest = doc.createElement("flightrequest");
		doc.appendChild(flightrequest);
		
		Attr attr = doc.createAttribute("id");
		attr.setValue(""+fr.getId());
		flightrequest.setAttributeNode(attr);
		
		Element arrivalAirportCode= doc.createElement("arrivalAirportCode");
		arrivalAirportCode.appendChild(doc.createTextNode(fr.getArrivalAirportCode()));
		flightrequest.appendChild(arrivalAirportCode);
		
		Element departureAirportCode= doc.createElement("departureAirportCode");
		departureAirportCode.appendChild(doc.createTextNode(fr.getDepartureAirportCode()));
		flightrequest.appendChild(departureAirportCode);
		
		Element noOfPassengers = doc.createElement("noOfPassengers");
		noOfPassengers.appendChild(doc.createTextNode("" + fr.getNoOfPassengers()));
		flightrequest.appendChild(noOfPassengers);
		
		Element status = doc.createElement("status");
		status.appendChild(doc.createTextNode(fr.getRequestStatus().toString()));
		flightrequest.appendChild(status);
		
		return XmlUtil.convertXmlDocumentToString(doc);

	}

}
