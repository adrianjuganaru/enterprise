package se.plushogskolan.jetbroker.order.rest.plane.model;

public class CreatePlaneResponse {
	long id;

	public CreatePlaneResponse(long id) {
		setId(id);
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}
}
