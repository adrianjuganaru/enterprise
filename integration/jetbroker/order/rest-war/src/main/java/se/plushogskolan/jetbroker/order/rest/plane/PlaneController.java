package se.plushogskolan.jetbroker.order.rest.plane;

import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import se.plushogskolan.jetbroker.order.domain.Plane;
import se.plushogskolan.jetbroker.order.rest.plane.model.CreatePlaneRequest;
import se.plushogskolan.jetbroker.order.rest.plane.model.CreatePlaneResponse;
import se.plushogskolan.jetbroker.order.rest.plane.model.GetPlaneResponse;
import se.plushogskolan.jetbroker.order.service.PlaneService;
import se.plushogskolan.jetbroker.order.rest.OkOrErrorResponse;

@Controller
public class PlaneController {

	@Autowired
	PlaneService planeService;
	Logger log = Logger.getLogger(PlaneController.class.getName());

	@RequestMapping(value = "/createPlane", method = RequestMethod.POST, produces = "application/json")
	@ResponseBody
	public CreatePlaneResponse createAirport(@RequestBody CreatePlaneRequest request) throws Exception {
		log.fine("createPlane: " + request);
		Plane plane = request.buildPlane();
		planeService.createPlane(plane);
		return new CreatePlaneResponse(plane.getId()); 
	}

	@RequestMapping(value = "/getPlane/{id}", method = RequestMethod.GET, produces = "application/json")
	@ResponseBody
	public GetPlaneResponse getPlane(@PathVariable long id) {
		
		return new GetPlaneResponse(planeService.getPlane(id));
	}

	@RequestMapping(value = "/deletePlane/{id}", method = RequestMethod.DELETE, produces = "application/json")
	@ResponseBody
	public OkOrErrorResponse deletePlane(@PathVariable long id) {
		
		try {
			planeService.deletePlane(id);
		
		return OkOrErrorResponse.getOkResponse();
		
		}catch (Exception e)
		{
		
			return OkOrErrorResponse.getErrorResponse(e.toString());
		}
		
	}

}
