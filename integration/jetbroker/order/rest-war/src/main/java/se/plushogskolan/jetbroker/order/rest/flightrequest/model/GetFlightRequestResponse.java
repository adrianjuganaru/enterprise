package se.plushogskolan.jetbroker.order.rest.flightrequest.model;

import se.plushogskolan.jetbroker.order.domain.FlightRequest;
import se.plushogskolan.jetbroker.order.rest.flightrequest.FlightRequestsController;

public class GetFlightRequestResponse {

	private long id;
	private long agentRequestId;
	private int noOfPassengers;
	private String departureAirportCode;
	private String arrivalAirportCode;
	private String date;
	private int distanceKm;
	private String status;

	public GetFlightRequestResponse(FlightRequest fr) {

		setId(fr.getAgentRequestId());
		setAgentRequestId(fr.getAgentRequestId());
		setNoOfPassengers(fr.getNoOfPassengers());
		setDepartureAirportCode(fr.getDepartureAirportCode());
		setArrivalAirportCode(fr.getArrivalAirportCode());
		setDate(fr.getDepartureTime().toString(FlightRequestsController.DATE_FORMAT));
		setDistanceKm(fr.getDistanceKm());
		setStatus(fr.getStatus().toString());
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public int getNoOfPassengers() {
		return noOfPassengers;
	}

	public void setNoOfPassengers(int noOfPassengers) {
		this.noOfPassengers = noOfPassengers;
	}

	public String getDepartureAirportCode() {
		return departureAirportCode;
	}

	public void setDepartureAirportCode(String departureAirportCode) {
		this.departureAirportCode = departureAirportCode;
	}

	public String getArrivalAirportCode() {
		return arrivalAirportCode;
	}

	public void setArrivalAirportCode(String arrivalAirportCode) {
		this.arrivalAirportCode = arrivalAirportCode;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public long getAgentRequestId() {
		return agentRequestId;
	}

	public void setAgentRequestId(long agentRequestId) {
		this.agentRequestId = agentRequestId;
	}

	public int getDistanceKm() {
		return distanceKm;
	}

	public void setDistanceKm(int distanceKm) {
		this.distanceKm = distanceKm;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	@Override
	public String toString() {
		return "GetFlightRequestResponse [id=" + id + ", agentRequestId=" + agentRequestId + ", noOfPassengers="
				+ noOfPassengers + ", departureAirportCode=" + departureAirportCode + ", arrivalAirportCode="
				+ arrivalAirportCode + ", date=" + date + ", distanceKm=" + distanceKm + ", status=" + status + "]";
	}

}
