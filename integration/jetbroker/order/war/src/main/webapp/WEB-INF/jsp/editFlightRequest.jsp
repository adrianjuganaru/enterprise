<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>


<html>
<head>
<link href="<%=request.getContextPath()%>/style/common.css"
	type="text/css" rel="stylesheet" />

</head>
<body>
	<jsp:include page="header.jsp" />

	<h2>Edit flight request</h2>

	<table class="formTable">
		<tr>
			<th>ID</th>
			<td>${flightRequest.id}</td>
		</tr>
		<tr>
			<th>Departure airport</th>
			<td>${departureAirport.code}, ${departureAirport.name} </td>
		</tr>
		<tr>
			<th>Arrival airport</th>
			<td>${arrivalAirport.code}, ${arrivalAirport.name}</td>
		</tr>
		<tr>
			<th>Date</th>
			<td><fmt:formatDate type="both" timeStyle="short"
					value="${flightRequest.departureTimeAsDate}" /></td>
		</tr>
		<tr>
			<th>No of passengers</th>
			<td>${flightRequest.noOfPassengers}</td>
		</tr>
		<tr>
			<th>Distance (km)</th>
			<td>${flightRequest.distanceKm}</td>
		</tr>
		<tr>
			<th>&nbsp;</th>
			<td></td>
		</tr>
		<tr>
			<th>Fuel cost (liter)</th>
			<td>${fuelCost}</td>
		</tr>
	</table>
	
	

	<h2>Our offer to customer</h2>

	<form:form commandName="editFlightRequestBean">

		<form:errors path="planeId" cssClass="errors" element="div"/>

		<table class="dataTable">
			<tr>
				<th></th>
				<th>Plane</th>
				<th>No of passengers</th>
				<th>Max speed (km/h)</th>
				<th>Fuel consumption (l/km)</th>
				<th>Total fuelcost</th>
				<th>Travel time (min)</th>
			</tr>
			<c:forEach items="${planeWrappers}" var="wrapper">
				<tr>
					<td>
						<form:radiobutton path="planeId" value="${wrapper.plane.id}"/>
					</td>
					<td>${wrapper.planeType.name}</td>
					<td>${wrapper.planeType.maxNoOfPassengers}</td>
					<td>${wrapper.planeType.maxSpeedKmH }</td>
					<td>${wrapper.planeType.fuelConsumptionPerKm }</td>
					<td><fmt:formatNumber value="${wrapper.fuelCost}" maxFractionDigits="0" /> SEK</td>
					<td>${wrapper.travelTimeMin}</td>
				</tr>

			</c:forEach>
		</table>
		

		<p>
			Offered price (SEK): <form:input path="price" /> <form:errors path="price" cssClass="errors" />
		<p>
		
		
		<p>	
			<input type="submit" value="Submit offer" />
			<a href="<%=request.getContextPath()%>/editFlightRequest/reject/${flightRequest.id}.html">Reject offer</a>
			<a href="<%=request.getContextPath()%>">Cancel</a>
		</p>
	</form:form>

</body>

</html>