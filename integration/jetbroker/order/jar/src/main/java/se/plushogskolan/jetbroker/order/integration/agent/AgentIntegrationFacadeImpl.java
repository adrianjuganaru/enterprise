package se.plushogskolan.jetbroker.order.integration.agent;

import java.util.logging.Logger;
import javax.annotation.Resource;
import javax.jms.MapMessage;
import javax.jms.Queue;
import javax.jms.QueueConnection;
import javax.jms.QueueConnectionFactory;
import javax.jms.QueueSender;
import javax.jms.QueueSession;
import javax.jms.Session;
import se.plushogskolan.jee.utils.cdi.Prod;
import se.plushogskolan.jee.utils.jms.JmsConstants;
import se.plushogskolan.jee.utils.jms.JmsHelper;
import se.plushogskolan.jetbroker.order.domain.FlightRequestConfirmation;

@Prod
public class AgentIntegrationFacadeImpl implements AgentIntegrationFacade {

	private static Logger log = Logger.getLogger(AgentIntegrationFacadeImpl.class.getName());
	
	@Resource(mappedName = JmsConstants.JNDI_CONNECTIONFACTORY)
	private QueueConnectionFactory connectionFactory;
	
	@Resource(mappedName = JmsConstants.QUEUE_FLIGHTREQUEST_RESPONSE)
	private Queue queue;
	
	private QueueConnection connection;
	private QueueSession senderSession;

	@Override
	public void sendFlightRequestConfirmation(FlightRequestConfirmation response) {
	
		log.fine("entering sendFlightRequestConfirmation");
		
		try{
		connection = connectionFactory.createQueueConnection();
		senderSession = connection.createQueueSession(false, Session.AUTO_ACKNOWLEDGE);
		connection.start();
		
		MapMessage message = senderSession.createMapMessage();
		
		message.setStringProperty("messageType", "Confirmation");
		message.setLongProperty("agentID", response.getAgentRequestId());
		message.setLongProperty("confirmationID", response.getOrderRequestId());
		
		
		QueueSender sender = senderSession.createSender(queue);
		sender.send(message);
		log.fine("sendFlightRequestConfirmation - the message is " + message.toString());
		
		}catch(Exception e){
			throw new RuntimeException(e);
		} finally {
			JmsHelper.closeConnectionAndSession(connection, senderSession);
		}
		
	}
	
		@Override
		public void sendUpdatedOfferMessage(
				se.plushogskolan.jetbroker.order.domain.FlightRequest flightRequest) {
			
		}
		
		@Override
		public void sendFlightRequestRejectedMessage(
				se.plushogskolan.jetbroker.order.domain.FlightRequest flightRequest) {
			
		}


}
