package se.plushogskolan.jetbroker.order.integration.MDB;

import java.text.SimpleDateFormat;
import java.util.logging.Logger;
import javax.ejb.ActivationConfigProperty;
import javax.ejb.MessageDriven;
import javax.inject.Inject;
import javax.jms.Message;
import javax.jms.MessageListener;
import org.joda.time.DateTime;
import se.plushogskolan.jee.utils.jms.AbstractMDB;
import se.plushogskolan.jee.utils.jms.JmsConstants;
import se.plushogskolan.jetbroker.order.domain.FlightRequest;
import se.plushogskolan.jetbroker.order.service.FlightRequestService;


@MessageDriven(name = "FlightRequestMDB", activationConfig = {
		@ActivationConfigProperty(propertyName = "destinationType", propertyValue = "javax.jms.Queue"),
		@ActivationConfigProperty(propertyName = "destination", propertyValue = JmsConstants.QUEUE_FLIGHTREQUEST_REQUEST),
		@ActivationConfigProperty(propertyName = "messageSelector", propertyValue = "messageType = 'flightRequestQueue'")

})
public class FlightRequestMDB extends AbstractMDB implements MessageListener {

	
	@Inject
	Logger log;
	
	@Inject
	private FlightRequestService flightRequestService;

	
	public FlightRequestMDB(){
		
	}
	public void onMessage(Message message) {
		FlightRequest flightRequest = new FlightRequest();


		log.fine("FlightRequestMDB: on Message. Entering on Message");
		log.fine("FlightRequestMDB: on Message. The message received is " + message.toString());
		try {
			int noOfPassengers = message.getIntProperty("noOfPassengers");
			String date =  message.getStringProperty("departureTime");
			log.fine("FlightRequestMDB: on Message. The date in date format is " + date);
			DateTime dateTime = new DateTime(new SimpleDateFormat("dd-MM-yyyy HH:mm:ss").parse(date));
			log.fine("FlightRequestMDB: on Message. The date in DateTime format is " + dateTime);
			
			long agentRequestId = message.getLongProperty("agentRequestId");
			log.fine("FlightRequestMDB: on Message. The agentRequestId is " + agentRequestId);
			String departureAirportCode = (String)message.getStringProperty("departureAirportCode");
			log.fine("FlightRequestMDB: on Message. The departure Airport Code is " + departureAirportCode);
			String arrivalAirportCode = (String) message.getStringProperty("arrivalAirportCode");
			log.fine("FlightRequestMDB: on Message. The arrival Airport Code is " + arrivalAirportCode);
			flightRequest.setNoOfPassengers(noOfPassengers);
			flightRequest.setDepartureTime(dateTime);
			flightRequest.setArrivalAirportCode(arrivalAirportCode);
			flightRequest.setDepartureAirportCode(departureAirportCode);
			flightRequest.setAgentRequestId(agentRequestId);
			log.fine("FlightRequestMDB: on Message. The flightrequest sent to service is " + flightRequest.toString());
				
			getFlightRequestService().handleIncomingFlightRequest(flightRequest);
		} catch (Exception e) {
			
			logErrorAndRollback(e);

		} 
		
		
	}
	public FlightRequestService getFlightRequestService() {
		return flightRequestService;
	}
	public void setFlightRequestService(FlightRequestService flightRequestService) {
		this.flightRequestService = flightRequestService;
	}

	
}
