package se.plushogskolan.jetbroker.order.repository;

import se.plushogskolan.jetbroker.order.domain.Airport;

public interface AirportRepository {
	Airport getAirport(String code);
}
