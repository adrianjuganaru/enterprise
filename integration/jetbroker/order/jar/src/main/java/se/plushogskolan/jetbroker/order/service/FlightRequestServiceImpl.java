package se.plushogskolan.jetbroker.order.service;

import java.util.List;
import java.util.logging.Logger;

import javax.ejb.Stateless;
import javax.inject.Inject;

import se.plushogskolan.jee.utils.cdi.Prod;
import se.plushogskolan.jee.utils.map.HaversineDistance;
import se.plushogskolan.jetbroker.order.domain.Airport;
import se.plushogskolan.jetbroker.order.domain.FlightRequest;
import se.plushogskolan.jetbroker.order.domain.FlightRequestConfirmation;
import se.plushogskolan.jetbroker.order.domain.FlightRequestStatus;
import se.plushogskolan.jetbroker.order.domain.Offer;
import se.plushogskolan.jetbroker.order.integration.agent.AgentIntegrationFacade;
import se.plushogskolan.jetbroker.order.repository.FlightRequestRepository;

@Stateless
public class FlightRequestServiceImpl implements FlightRequestService {
	private static Logger log = Logger.getLogger(FlightRequestServiceImpl.class.getName());

	@Inject
	private FlightRequestRepository flightRequestRepository;

	@Inject
	private PlaneService planeService;

	@Inject 
	@Prod
	private AgentIntegrationFacade agentIntegrationFacade;

	@Override
	public FlightRequestConfirmation handleIncomingFlightRequest(FlightRequest request) {

		log.fine("FlightRequestService : handleIncomingFlightRequest - entering: " + request);
		Airport airport1 = planeService.getAirport(request.getDepartureAirportCode());
		Airport airport2 = planeService.getAirport(request.getArrivalAirportCode());
		
	
		request.setStatus(FlightRequestStatus.NEW);
		request.setDistanceKm((int) HaversineDistance.getDistance(airport1.getLatitude(), airport1.getLongitude(), airport2.getLatitude(), airport2.getLongitude()));
		
		log.fine("FlightRequestService : handleIncomingFlightRequest - flightRequest is " + request.toString());
		
		long persistId = flightRequestRepository.persist(request);
		FlightRequestConfirmation confirmation = new FlightRequestConfirmation(request.getAgentRequestId(), persistId);
		agentIntegrationFacade.sendFlightRequestConfirmation(confirmation);
		return confirmation;

	}

	@Override
	public void handleUpdatedOffer(long flightRequestId, Offer offer) {

		// Implement
	}

	@Override
	public void rejectFlightRequest(long id) {

		// Implement

	}

	@Override
	public FlightRequest getFlightRequest(long id) {
		return getFlightRequestRepository().findById(id);
	}

	@Override
	public List<FlightRequest> getAllFlightRequests() {
		return getFlightRequestRepository().getAllFlightRequests();
	}

	@Override
	public void deleteFlightRequest(long id) {
		FlightRequest flightRequest = getFlightRequest(id);
		getFlightRequestRepository().remove(flightRequest);

	}

	public FlightRequestRepository getFlightRequestRepository() {
		return flightRequestRepository;
	}

	public void setFlightRequestRepository(FlightRequestRepository flightRequestRepository) {
		this.flightRequestRepository = flightRequestRepository;
	}

	public PlaneService getPlaneService() {
		return planeService;
	}

	public void setPlaneService(PlaneService planeService) {
		this.planeService = planeService;
	}

	public AgentIntegrationFacade getAgentIntegrationFacade() {
		return agentIntegrationFacade;
	}

	public void setAgentIntegrationFacade(AgentIntegrationFacade agentIntegrationFacade) {
		this.agentIntegrationFacade = agentIntegrationFacade;
	}

}
