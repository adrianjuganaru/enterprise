package se.plushogskolan.jetbroker.order.repository.jpa;

import java.util.List;

import se.plushogskolan.jee.utils.repository.jpa.JpaRepository;
import se.plushogskolan.jetbroker.order.domain.FlightRequest;
import se.plushogskolan.jetbroker.order.repository.FlightRequestRepository;

public class JpaFlightRequestRepository extends JpaRepository<FlightRequest> implements FlightRequestRepository {

	@SuppressWarnings("unchecked")
	@Override
	public List<FlightRequest> getAllFlightRequests() {
		return em.createQuery("select r from FlightRequest r").getResultList();
	}

}
