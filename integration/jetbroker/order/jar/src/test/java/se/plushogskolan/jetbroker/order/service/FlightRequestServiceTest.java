package se.plushogskolan.jetbroker.order.service;

import static org.junit.Assert.*;

import org.easymock.Capture;

import static org.easymock.EasyMock.*;

import org.junit.Before;
import org.junit.Test;

import se.plushogskolan.jetbroker.order.domain.Airport;
import se.plushogskolan.jetbroker.order.domain.FlightRequestConfirmation;
import se.plushogskolan.jetbroker.order.domain.FlightRequestStatus;
import se.plushogskolan.jetbroker.order.repository.FlightRequestRepository;
import se.plushogskolan.jetbroker.order.TestFixture;
import se.plushogskolan.jetbroker.order.domain.FlightRequest;
import se.plushogskolan.jetbroker.order.integration.agent.AgentIntegrationFacade;

public class FlightRequestServiceTest {
	
	private FlightRequest flightRequest;

	
	@Before
	public void setup() throws InstantiationException, IllegalAccessException{
		flightRequest = TestFixture.getValidFlightRequest();
		
}
	
	@Test
	public void testHandleIncomingFlightRequest(){
		
		flightRequest.setStatus(FlightRequestStatus.BOOKED); //set different status for testing purposes
		Airport airport1 = new Airport("STM", "Stockholm [Mocked]", 59.33, 18.06);
		Airport airport2 = new Airport("GBG", "Gothenburg [Mocked]", 57.697, 11.98);
		Capture<FlightRequest> capturedFlightRequest = new Capture<FlightRequest>();
		Capture<FlightRequestConfirmation> capturedFlightRequestConfirmation = new Capture<FlightRequestConfirmation>();
			
		PlaneService planeServiceMock = createMock(PlaneService.class);
		expect(planeServiceMock.getAirport(flightRequest.getDepartureAirportCode())).andReturn(airport1);
		expect(planeServiceMock.getAirport(flightRequest.getArrivalAirportCode())).andReturn(airport2);
		replay(planeServiceMock);
		
		FlightRequestRepository flightRequestRepositoryMock = createMock(FlightRequestRepository.class);
		expect(flightRequestRepositoryMock.persist(capture(capturedFlightRequest))).andReturn(1L);
		replay(flightRequestRepositoryMock);
				
		AgentIntegrationFacade agentIntegrationFacadeMock = createMock(AgentIntegrationFacade.class);
		agentIntegrationFacadeMock.sendFlightRequestConfirmation(capture(capturedFlightRequestConfirmation));
		replay(agentIntegrationFacadeMock);
		
		FlightRequestServiceImpl flightRequestService = new FlightRequestServiceImpl();
		flightRequestService.setPlaneService(planeServiceMock);
		flightRequestService.setFlightRequestRepository(flightRequestRepositoryMock);
		flightRequestService.setAgentIntegrationFacade(agentIntegrationFacadeMock);
		
		flightRequestService.handleIncomingFlightRequest(flightRequest);
		
		verify(planeServiceMock);
		verify(flightRequestRepositoryMock);
		verify(agentIntegrationFacadeMock);
		
		assertEquals(FlightRequestStatus.NEW, capturedFlightRequest.getValue().getStatus());
		assertEquals(10, capturedFlightRequestConfirmation.getValue().getAgentRequestId());
		assertEquals(1, capturedFlightRequestConfirmation.getValue().getOrderRequestId());
	}
}
