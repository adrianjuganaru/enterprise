package se.plushogskolan.jetbroker.plane.rest.planetype;

import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import se.plushogskolan.jetbroker.plane.domain.PlaneType;
import se.plushogskolan.jetbroker.plane.rest.OkOrErrorResponse;
import se.plushogskolan.jetbroker.plane.rest.airport.AirportController;
import se.plushogskolan.jetbroker.plane.rest.planetype.model.CreatePlaneTypeRequest;
import se.plushogskolan.jetbroker.plane.rest.planetype.model.CreatePlaneTypeResponse;
import se.plushogskolan.jetbroker.plane.rest.planetype.model.GetPlaneTypeResponse;
import se.plushogskolan.jetbroker.plane.service.PlaneService;

@Controller
public class PlaneTypeController {
	

	@Autowired
	PlaneService planeTypeService;

	Logger log = Logger.getLogger(AirportController.class.getName());
	
	@RequestMapping(value = "/createPlaneType", method = RequestMethod.POST, produces = "application/json")
	@ResponseBody
	public CreatePlaneTypeResponse createPlaneType(@RequestBody CreatePlaneTypeRequest request) throws Exception {
		log.fine("createPlaneType: " + request);
		PlaneType planeType = request.buildPlaneType();
		planeTypeService.createPlaneType(planeType);
		return new CreatePlaneTypeResponse(planeType.getId()); 
	}
	
	@RequestMapping(value = "/getPlaneType/{id}", method = RequestMethod.GET, produces = "application/json")
	@ResponseBody
	public GetPlaneTypeResponse getPlaneType(@PathVariable long id) {
		
		return new GetPlaneTypeResponse(planeTypeService.getPlaneType(id));
	}
	
	@RequestMapping(value = "/deletePlaneType/{id}", method = RequestMethod.DELETE, produces = "application/json")
	@ResponseBody
	public OkOrErrorResponse deletePlaneType(@PathVariable long id) {
		
		try {
			planeTypeService.deletePlaneType(id);
		
		return OkOrErrorResponse.getOkResponse();
		
		}catch (Exception e)
		{
		
			return OkOrErrorResponse.getErrorResponse(e.toString());
		}
		
	}

	

}
