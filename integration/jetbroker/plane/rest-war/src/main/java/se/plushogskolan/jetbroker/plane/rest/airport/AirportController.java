package se.plushogskolan.jetbroker.plane.rest.airport;

import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import se.plushogskolan.jetbroker.plane.domain.Airport;
import se.plushogskolan.jetbroker.plane.rest.OkOrErrorResponse;
import se.plushogskolan.jetbroker.plane.rest.airport.model.CreateAirportRequest;
import se.plushogskolan.jetbroker.plane.rest.airport.model.CreateAirportResponse;
import se.plushogskolan.jetbroker.plane.rest.airport.model.GetAirportResponse;
import se.plushogskolan.jetbroker.plane.service.AirportService;
@Controller
public class AirportController {

	@Autowired
	AirportService airportService;

	Logger log = Logger.getLogger(AirportController.class.getName());

	@RequestMapping(value = "/createAirport", method = RequestMethod.POST, produces = "application/json")
	@ResponseBody
	public CreateAirportResponse createAirport(@RequestBody CreateAirportRequest request) throws Exception {
		log.fine("createAirport: " + request);
		Airport airport = request.buildAirport();
		airportService.createAirport(airport);
		return new CreateAirportResponse(airport.getId()); 
	}

	@RequestMapping(value = "/getAirport/{id}", method = RequestMethod.GET, produces = "application/json")
	@ResponseBody
	public GetAirportResponse getAirport(@PathVariable long id) {
		
		return new GetAirportResponse(airportService.getAirport(id));
	}

	@RequestMapping(value = "/deleteAirport/{id}", method = RequestMethod.DELETE, produces = "application/json")
	@ResponseBody
	public OkOrErrorResponse deleteAirport(@PathVariable long id) {
		
		try {
			airportService.deleteAirport(id);
		
		return OkOrErrorResponse.getOkResponse();
		
		}catch (Exception e)
		{
		
			return OkOrErrorResponse.getErrorResponse(e.toString());
		}
		
	}

	@RequestMapping(value = "/updateFuelPrice/{fuelPrice}", method = RequestMethod.PUT, produces = "application/json")
	@ResponseBody
	public OkOrErrorResponse updateFuelPrice(@PathVariable double fuelPrice) {

		try {
			airportService.updateFuelCost(fuelPrice);
			return OkOrErrorResponse.getOkResponse();
		} catch (Exception e)
		{
		
			return OkOrErrorResponse.getErrorResponse(e.toString());
		}
	}

}
