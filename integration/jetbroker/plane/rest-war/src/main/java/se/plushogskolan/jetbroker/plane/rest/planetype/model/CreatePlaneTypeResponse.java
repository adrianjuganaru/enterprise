package se.plushogskolan.jetbroker.plane.rest.planetype.model;

public class CreatePlaneTypeResponse {
	long id;

	public CreatePlaneTypeResponse(long id) {
		setId(id);
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}
}
