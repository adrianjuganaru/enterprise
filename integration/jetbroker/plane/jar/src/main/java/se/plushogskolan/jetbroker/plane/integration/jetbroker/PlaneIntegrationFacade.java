package se.plushogskolan.jetbroker.plane.integration.jetbroker;


public interface PlaneIntegrationFacade {
	
	public void broadcastNewFuelPrice(double fuelPrice);

	public void broadcastAirportsChanged();

	public void broadcastPlaneTypesChanged();

}
