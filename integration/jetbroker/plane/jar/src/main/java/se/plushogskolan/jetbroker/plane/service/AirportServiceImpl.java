package se.plushogskolan.jetbroker.plane.service;

import java.util.Collections;
import java.util.List;
import java.util.logging.Logger;

import javax.ejb.Stateless;
import javax.inject.Inject;

import se.plushogskolan.jee.utils.cdi.Prod;
import se.plushogskolan.jetbroker.plane.domain.Airport;
import se.plushogskolan.jetbroker.plane.integration.jetbroker.PlaneIntegrationFacade;
import se.plushogskolan.jetbroker.plane.repository.AirportRepository;

@Stateless
public class AirportServiceImpl implements AirportService {

	@Inject
	Logger log;
	
	@Inject
	private AirportRepository airportRepo;
	
	@Inject
	@Prod
	private PlaneIntegrationFacade planeIntegrationFacade;
	
	@Inject
	private FuelPriceService fuelPriceService;

	@Override
	public Airport getAirport(long id) {
		return getAirportRepo().findById(id);
	}

	@Override
	public Airport getAirportByCode(String code) {
		return getAirportRepo().getAirportByCode(code);
	}

	@Override
	public void updateAirport(Airport airport) {
		getAirportRepo().update(airport);
	}

	@Override
	public Airport createAirport(Airport airport) {
		long id = getAirportRepo().persist(airport);
		return getAirport(id);
	}

	@Override
	public List<Airport> getAllAirports() {
		List<Airport> airports = getAirportRepo().getAllAirports();
		Collections.sort(airports);
		return airports;
	}

		@Override
	public void deleteAirport(long id) {
		Airport airport = getAirport(id);
		getAirportRepo().remove(airport);
	}

	@Override
	public void updateAirportsFromWebService() {
		// Implement
	}

	public AirportRepository getAirportRepo() {
		return airportRepo;
	}

	public void setAirportRepo(AirportRepository airportRepo) {
		this.airportRepo = airportRepo;
	}

	@Override
	public double getFuelCost() {
		return fuelPriceService.getFuelCost();
	}

	@Override
	public void updateFuelCost(double fuelCost) {
		fuelPriceService.updateFuelCost(fuelCost);
		planeIntegrationFacade.broadcastNewFuelPrice(fuelCost);
		
	}

}
