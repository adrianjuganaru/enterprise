package se.plushogskolan.jetbroker.plane.service;

import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;

import se.plushogskolan.jetbroker.plane.domain.PlaneType;
import se.plushogskolan.jetbroker.plane.repository.PlaneTypeRepository;

@Stateless
public class PlaneServiceImpl implements PlaneService {

	@Inject
	private PlaneTypeRepository planeRepository;

	@Override
	public PlaneType getPlaneType(long id) {
		return getPlaneRepository().findById(id);
	}

	@Override
	public PlaneType createPlaneType(PlaneType planeType) {
		long id = getPlaneRepository().persist(planeType);
		return getPlaneType(id);
	}

	@Override
	public void updatePlaneType(PlaneType planeType) {
		getPlaneRepository().update(planeType);
	}

	@Override
	public List<PlaneType> getAllPlaneTypes() {
		return getPlaneRepository().getAllPlaneTypes();
	}

	@Override
	public void deletePlaneType(long id) {
		PlaneType planeType = getPlaneType(id);
		getPlaneRepository().remove(planeType);
	}

	public PlaneTypeRepository getPlaneRepository() {
		return planeRepository;
	}

	public void setPlaneRepository(PlaneTypeRepository planeRepository) {
		this.planeRepository = planeRepository;
	}

}
