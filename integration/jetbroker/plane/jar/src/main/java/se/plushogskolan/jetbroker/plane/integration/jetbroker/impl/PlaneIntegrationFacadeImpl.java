package se.plushogskolan.jetbroker.plane.integration.jetbroker.impl;

import java.util.logging.Logger;

import javax.annotation.Resource;
import javax.inject.Inject;
import javax.jms.Message;
import javax.jms.Session;
import javax.jms.Topic;
import javax.jms.TopicConnection;
import javax.jms.TopicConnectionFactory;
import javax.jms.TopicPublisher;
import javax.jms.TopicSession;

import se.plushogskolan.jee.utils.cdi.Prod;
import se.plushogskolan.jee.utils.jms.JmsConstants;
import se.plushogskolan.jee.utils.jms.JmsHelper;
import se.plushogskolan.jetbroker.plane.integration.jetbroker.PlaneIntegrationFacade;

@Prod
public class PlaneIntegrationFacadeImpl implements PlaneIntegrationFacade {

	@Inject
	Logger log;
	
	@Resource(mappedName = JmsConstants.JNDI_CONNECTIONFACTORY)
	private TopicConnectionFactory connectionFactory;
	
	@Resource(mappedName = JmsConstants.TOPIC_PLANE_BROADCAST)
	private Topic topic;
	

	@Override
	public void broadcastNewFuelPrice(double fuelPrice) {
		TopicConnection connection = null;
		TopicSession pubSession = null;
		log.fine("IMPL: entering broadcastNewFuelPrice.");

		try{
		connection = connectionFactory.createTopicConnection();
		pubSession = connection.createTopicSession(false, Session.AUTO_ACKNOWLEDGE); 
		connection.start();
		
		Message message = pubSession.createMessage();
		message.setDoubleProperty("fuelPrice", fuelPrice);
		log.fine("IMPL: broadcastNewFuelPrice. Message is " + message.getDoubleProperty("fuelPrice"));
		message.setStringProperty("messageType", JmsConstants.MSGTYPE_PLANEBROADCAST_FUELPRICECHANGED);
		
		log.fine("IMPL: broadcastNewFuelPrice. New price is " + fuelPrice);
		TopicPublisher publisher = pubSession.createPublisher(topic);
		publisher.publish(message);
		log.fine("IMPL: broadcastNewFuelPrice. Broadcast is sent");
		
		}catch(Exception e){
			throw new RuntimeException(e);
		} finally {
			JmsHelper.closeConnectionAndSession(connection, pubSession);
		}
		
	}

	@Override
	public void broadcastAirportsChanged() {
		log.info("IMPL: Airports changed");
	}

	@Override
	public void broadcastPlaneTypesChanged() {
		log.info("IMPL: Plane types changed");

	}

}
