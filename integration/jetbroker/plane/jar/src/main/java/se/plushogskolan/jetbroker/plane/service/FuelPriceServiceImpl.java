package se.plushogskolan.jetbroker.plane.service;

import javax.ejb.ConcurrencyManagement;
import javax.ejb.ConcurrencyManagementType;
import javax.ejb.Lock;
import javax.ejb.LockType;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import se.plushogskolan.jetbroker.plane.service.FuelPriceService;

@Startup
@Singleton
@ConcurrencyManagement(ConcurrencyManagementType.CONTAINER)
public class FuelPriceServiceImpl implements FuelPriceService {


	private double cachedFuelCost = 5.2;

	@Lock(LockType.READ)
	public double getFuelCost() {
		return cachedFuelCost;
	}

	@Lock(LockType.WRITE)
	public void updateFuelCost(double fuelCost) {
		cachedFuelCost = fuelCost;
	}

	
}
