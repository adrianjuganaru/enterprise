package se.plushogskolan.jetbroker.plane.integration.jetbroker.ws;

import java.util.List;

import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.jws.soap.SOAPBinding.Style;

@WebService()
@SOAPBinding(style = Style.RPC)
public interface PlaneWebService {

	@WebMethod()
	public List<WSAirport> getAirports();

	@WebMethod()
	public List<WSPlaneType> getPlaneTypes();

}
