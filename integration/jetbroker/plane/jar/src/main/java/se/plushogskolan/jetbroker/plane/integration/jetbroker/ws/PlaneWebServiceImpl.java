package se.plushogskolan.jetbroker.plane.integration.jetbroker.ws;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.jws.WebService;

import se.plushogskolan.jetbroker.plane.domain.Airport;
import se.plushogskolan.jetbroker.plane.domain.PlaneType;
import se.plushogskolan.jetbroker.plane.service.AirportService;
import se.plushogskolan.jetbroker.plane.service.PlaneService;

@Stateless
@WebService(name="PlaneWebService", endpointInterface = "se.plushogskolan.jetbroker.plane.integration.jetbroker.ws.PlaneWebService")
public class PlaneWebServiceImpl implements PlaneWebService{
	
	@Inject
	PlaneService planeService;
	
	@Inject
	AirportService airportService;
	
	@Inject
	Logger log;

	@Override
		public List<WSAirport> getAirports() {
		
		List<WSAirport> wsAirportList = new ArrayList<WSAirport>();
		List<Airport> allAirports = airportService.getAllAirports();
		
		for(Airport airport : allAirports){
			WSAirport wsAirport = new WSAirport();
			wsAirport.setCode(airport.getCode());
			wsAirport.setName(airport.getName());
			wsAirport.setLatitude(airport.getLatitude());
			wsAirport.setLongitude(airport.getLongitude());
			log.fine("Plane System: PlaneWebServiceImpl - iteration - airport added:  " + wsAirport.toString());
			wsAirportList.add(wsAirport);
			log.fine("Plane System: PlaneWebServiceImpl - iteration wsAirportList: " + wsAirportList.toString());

		}
		log.fine("Plane System: PlaneWebServiceImpl - getAirports returns wsAirportList: " + wsAirportList.toString());
		return wsAirportList;
	}

	@Override
	public List<WSPlaneType> getPlaneTypes() {
		
		List<WSPlaneType> wsPlaneTypeList = new ArrayList<WSPlaneType>();
		
		List<PlaneType> allPlaneTypes = planeService.getAllPlaneTypes();
		WSPlaneType wsPlaneType = new WSPlaneType();
		
		for(PlaneType planeType : allPlaneTypes ){
			wsPlaneType.setCode(planeType.getCode());
			wsPlaneType.setName(planeType.getName());
			wsPlaneType.setFuelConsumptionPerKm(planeType.getFuelConsumptionPerKm());
			wsPlaneType.setMaxSpeedKmH(planeType.getMaxSpeedKmH());
			wsPlaneType.setMaxNoOfPassengers(planeType.getMaxNoOfPassengers());
			wsPlaneType.setMaxRangeKm(planeType.getMaxRangeKm());
			wsPlaneTypeList.add(wsPlaneType);
		}
		
		return wsPlaneTypeList;
	}

}
