package webservices.helloworld;

import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.jws.soap.SOAPBinding.Style;

/**
 * Add annotations to make this a web service
 */
@WebService
@SOAPBinding(style = Style.RPC)
public interface HelloWorld {

	@WebMethod
	public String helloWorld();

}
