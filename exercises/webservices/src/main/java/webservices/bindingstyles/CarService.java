package webservices.bindingstyles;

import javax.jws.WebService;

@WebService
public interface CarService {
	public Car getCar(RegNumber regNo);
}
