package webservices.bindingstyles.rpc;

import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.jws.soap.SOAPBinding.Style;

import webservices.bindingstyles.CarService;

/**
 * Add annotations to make this an RPC style web service
 */
@WebService
@SOAPBinding(style=Style.RPC)
public interface CarServiceRpcStyle extends CarService {

}
