package se.plushogskolan.cdi.repository;

import se.plushogskolan.cdi.model.Car;

public interface CarRepository {

	public Car getNormalCar();

	public Car getSportsCar();

	public void saveCar(Car car);

}
