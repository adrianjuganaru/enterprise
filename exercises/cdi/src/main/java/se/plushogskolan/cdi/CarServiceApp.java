package se.plushogskolan.cdi;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.event.Observes;
import javax.inject.Inject;
import javax.inject.Singleton;

import se.plushogskolan.cdi.annotations.Environment;
import se.plushogskolan.cdi.annotations.Environment.Scope;
import se.plushogskolan.cdi.annotations.Income;
import se.plushogskolan.cdi.annotations.Income.IncomeLevel;
import se.plushogskolan.cdi.annotations.TodaysDate;
import se.plushogskolan.cdi.annotations.YesterdaysDate;
import se.plushogskolan.cdi.model.Car;
import se.plushogskolan.cdi.model.Owner;
import se.plushogskolan.cdi.services.CarService;
/**
 * A class that represents an interface to an Application. A way of collecting
 * variables that our testcase wants to use.
 * 
 */
@Singleton
public class CarServiceApp {
	
	@Inject @Environment(Scope.MOCK) @Income(IncomeLevel.MEDIUM)
	protected CarService normalCarService;
	@Inject @Environment(Scope.PROD) @Income(IncomeLevel.HIGH)
	protected CarService sportCarService;
	@Inject @TodaysDate
	protected String todaysDate;
	@Inject @YesterdaysDate
	protected String yesterdaysDate;
	
	@Inject @Income(IncomeLevel.MEDIUM)
	protected Owner normalUser1;
	@Inject @Income(IncomeLevel.MEDIUM)
	protected Owner normalUser2;
	@Inject @Income(IncomeLevel.HIGH)
	protected Owner richUser1;
	@Inject @Income(IncomeLevel.HIGH)
	protected Owner richUser2;

	protected static int carUpdatedCount;

	/**
	 * Callback when a car is updated in the production database. The event is
	 * fired in DbCarRepository.
	 */
	public void onCarUpdated(@Observes Car car) {
		carUpdatedCount++;
		System.out.println("Event: car updated in production database, name="
				+ car.getName() + ". No of car events: " + carUpdatedCount);
	}

}
