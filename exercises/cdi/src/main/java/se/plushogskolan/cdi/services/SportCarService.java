package se.plushogskolan.cdi.services;

import javax.inject.Inject;

import se.plushogskolan.cdi.annotations.Environment;
import se.plushogskolan.cdi.annotations.Income;
import se.plushogskolan.cdi.annotations.Environment.Scope;
import se.plushogskolan.cdi.annotations.Income.IncomeLevel;
import se.plushogskolan.cdi.annotations.InvocationCounter;
import se.plushogskolan.cdi.model.Car;
import se.plushogskolan.cdi.model.Owner;
import se.plushogskolan.cdi.repository.CarRepository;

@Environment(Scope.PROD)
@Income(IncomeLevel.HIGH)
@InvocationCounter
public class SportCarService implements CarService {

	/**
	 * Injecting a high income owner. The Owner is created using the @Producer
	 * method in Owner.java.
	 */
	@Inject @Income(IncomeLevel.HIGH)
	Owner owner;

	@Inject @Environment(Scope.PROD)
	private CarRepository carRepository;

	public Car getCar() {
		Car car = carRepository.getSportsCar();
		car.setOwner(owner);
		return car;
	}

	public void saveCar(Car car) {
		carRepository.saveCar(car);

	}

	@Override
	public String toString() {
		return "SportCarService, owner=" + owner;
	}

}
