package se.plushogskolan.cdi.repository.db;

import javax.enterprise.event.Event;
import javax.enterprise.inject.Alternative;
import javax.enterprise.inject.Any;
import javax.inject.Inject;

import se.plushogskolan.cdi.annotations.Environment;
import se.plushogskolan.cdi.annotations.InvocationCounter;
import se.plushogskolan.cdi.annotations.Environment.Scope;
import se.plushogskolan.cdi.model.Car;
import se.plushogskolan.cdi.repository.CarRepository;

/**
 * A repository to be used in the production environment
 * 
 */
@Environment(Scope.PROD)
@InvocationCounter
public class DbCarRepository implements CarRepository {

	public DbCarRepository() {
	}

	public Car getNormalCar() {
		return new Car("DB normal car");
	}

	public Car getSportsCar() {
		return new Car("DB sports car");
	}
	
	@Inject @Any Event<Car> carEvent;
	public void saveCar(Car car) {
		System.out.println("Save car in DB: " + car.getName());
		carEvent.fire(car);
	}

}
